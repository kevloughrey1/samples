package eu.transformproject.clinicalevidenceservice.web;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.UpdateExecutionException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.http.HTTPRepository;

import eu.transformproject.clinicalevidenceservice.business.Constants;

/**
 * Update a given CPR's journal paper info. 
 * Defines and executes SPARQL queries to do the above.
 *
 * @author Kevin Loughrey
 */

@Path("/query/updatepaper")
public class UpdatePaper
{
	@POST
	@Produces({MediaType.TEXT_PLAIN})
	public Response getXML(@FormParam("json") String jsonStr) throws RDF4JException 
	{
		HTTPRepository myRepository = new HTTPRepository(Constants.sesameServer, Constants.repositoryID);
		try 
		{
			// Attempt to connect to the clinical evidence ontology
			myRepository.initialize();
		}
		catch (RepositoryException e1) 
		{
			// If unable to connect to the ontology, print a stack trace and terminate
			e1.printStackTrace();
		}
		RepositoryConnection con = myRepository.getConnection();
		
		JSONObject obj = new JSONObject(jsonStr);
		String cprID = obj.getString("cprID");
		String evidenceID = obj.getString("evidenceID");
		String cprRegisterTitleOld = obj.getString("cprRegisterTitleOld");
		String journalPaperTitleOld = obj.getString("journalPaperTitleOld");
		String authorsOld = obj.getString("authorsOld");
		String journalOld = obj.getString("journalOld");
		String publicationYearOld = obj.getString("publicationYearOld");
		String urlOld = obj.getString("urlOld");
		String articleTypeOld = obj.getString("articleTypeOld");
		String clinicalDomainOld = obj.getString("clinicalDomainOld");
		String cprRegisterTitle = obj.getString("cprRegisterTitle");
		String journalPaperTitle = obj.getString("journalPaperTitle");
		String authors = obj.getString("authors");
		String journal = obj.getString("journal");
		String publicationYear = obj.getString("publicationYear");
		String url = obj.getString("url");
		String articleType = obj.getString("articleType")	;
		String clinicalDomain = obj.getString("clinicalDomain");
		String searchableCprNameOld = cprRegisterTitleOld.replaceAll("[^A-Za-z0-9 ]", "");
        String searchableCprName = cprRegisterTitle.replaceAll("[^A-Za-z0-9 ]", "");
        
        String articleTypeIDOld = articleTypeOld.equals("Original article") ? "1" : "2";
        String articleTypeID = articleType.equals("Original article") ? "1" : "2";
		/*// Replace spaces with underscores as the ontology is set up this way
		query = query.replace(" ", "_");
		
		// Parse the search query to SPARQL format.
		SearchBuilder sB = new SearchBuilder(query);
		String sparqlQuery = sB.ParseQuery();
		
		// Restore any bracket characters which were modified by the search builder.
		sparqlQuery = sparqlQuery.replaceAll("&obkt;", "(");
		sparqlQuery = sparqlQuery.replaceAll("&cbkt;", ")");
		sparqlQuery = sparqlQuery.replaceAll("_", " ");*/
		
		// Form the full search query.
		String finalQuery = Constants.queryString + '\n' + 
			"DELETE { \n" + 
			  "?anyCPR CPRs:hasSearchableName \"" + searchableCprNameOld + "\"^^xsd:string . \n " +
			  "?anyCPR CPRs:hasRegisterTitle \"" + cprRegisterTitleOld + "\"^^xsd:string . \n" + 
			  "?Article CPRs:hasArticleName \"" + journalPaperTitleOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasYear \"" + publicationYearOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasURL \"" + urlOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasJournal \"" + journalOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleAuthor \"" + authorsOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleTypeID \"" + articleTypeIDOld + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleTypeName \"" + articleTypeOld + "\"^^xsd:string .\n " +
			  "?instance CPRs:hasDomain CPRs:" + clinicalDomainOld + " .\n " +
			"}\n " +
			"INSERT {\n " + 
			  "?anyCPR CPRs:hasSearchableName \"" + searchableCprName + "\"^^xsd:string . \n " +
			  "?anyCPR CPRs:hasRegisterTitle \"" + cprRegisterTitle + "\"^^xsd:string . \n" + 
			  "?Article CPRs:hasArticleName \"" + journalPaperTitle + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasYear \"" + publicationYear + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasURL \"" + url + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasJournal \"" + journal + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleAuthor \"" + authors + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleTypeID \"" + articleTypeID + "\"^^xsd:string .\n " +
			  "?Article CPRs:hasArticleTypeName \"" + articleType + "\"^^xsd:string .\n " +
			  "?instance CPRs:hasDomain CPRs:" + clinicalDomain + " .\n " +
			"}\n " +
			"WHERE {\n " +
			"  ?anyCPR CPRs:hasIDInstance ?instance .\n " +
			"  ?anyCPR CPRs:hasCPRID \"" + cprID + "\"^^xsd:string .\n " +
			"  ?instance CPRs:hasEvidenceID \"" + evidenceID + "\"^^xsd:string .\n " +
			"  ?instance CPRs:hasArticle ?Article .\n " +
			"};";
	//	System.out.print(finalQuery);
//		return Response.ok(finalQuery).build();

		try {
			Update update = con.prepareUpdate(QueryLanguage.SPARQL, finalQuery);
			update.execute();
		}
		catch (RepositoryException e) {
			e.printStackTrace();
		}
		catch (MalformedQueryException e)
		{
			e.printStackTrace();
		}
		catch (UpdateExecutionException e) {
			e.printStackTrace();
		}
		con.commit();
		con.close();
		myRepository.shutDown();
		return Response.ok("Update successful. Please reload the page to view changes.").build();
	}
}