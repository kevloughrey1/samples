#!/bin/sh
if [ $# -eq 0 ]; then
    echo "Default build"
    docker build -t javabase .
    docker-compose up tomcat
	
fi

if [ "$1" == "rebuild" ]; then
    echo "Rebuild keeping tomcat running"
    docker-compose build --no-cache java
    docker-compose up -d java
	rm -rf docker_tomcat_1:/usr/local/tomcat/webapps/ClinicalEvidenceRESTService
	rm -f docker_tomcat_1:/usr/local/tomcat/webapps/ClinicalEvidenceRESTService.war
	docker cp docker_java_1:/home/ClinicalEvidenceRESTServiceProject/ClinicalEvidenceRESTService/ClinicalEvidenceRESTService.war . 
	docker cp ClinicalEvidenceRESTService.war docker_tomcat_1:/usr/local/tomcat/webapps/
	docker stop docker_java_1
fi
