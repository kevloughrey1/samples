function byScore(a, b) {
    return b['result']['score']['raw'] - a['result']['score']['raw'];
}
var data;
var page = 1;
var pageMax;
var numResults;
var numPerPage = 5;
function openLeaderboard() {
    document.getElementById("leaderboardOverlay").style.height = "100%";

    queryCompletedCourse(_COURSE_URL, function () {
        numResults = this.length;
        pageMax = Math.floor(numResults/numPerPage) + 1;
        this.sort(byScore);
        var maxRes = (numResults < (page * numPerPage)) ? numResults : (page * numPerPage);
        data = this;
        
        var board = document.getElementById("leaderboard");
        for (var i = 0; i < maxRes; i++) {
            var row = document.createElement("li");
            
            var num = document.createElement("num");
            var numText = document.createTextNode(i + 1);
            num.appendChild(numText);
            row.appendChild(num);
 
            var name = document.createElement("name");
            var nameText = document.createTextNode(this[i].actor.name);
            name.appendChild(nameText);
            row.appendChild(name);

            var score = document.createElement("score");
            var scoreText = document.createTextNode(this[i].result.score.raw);
            score.appendChild(scoreText);
            row.appendChild(score); 

            board.appendChild(row);
        }

    });
}

function nextPage() {
    if (page < pageMax) {
        page++;
    }
    var board = document.getElementById("leaderboard");
    while (board.firstChild) {
        board.removeChild(board.firstChild);
    }
    var maxRes = (numResults < (page * numPerPage)) ? numResults : (page * numPerPage);
    var minRes = (page * numPerPage) - numPerPage;
    for (var i = minRes; i < maxRes; i++) {
        var row = document.createElement("li");

        var num = document.createElement("num");
        var numText = document.createTextNode(i + 1);
        num.appendChild(numText);
        row.appendChild(num);

        var name = document.createElement("name");
        var nameText = document.createTextNode(data[i].actor.name);
        name.appendChild(nameText);
        row.appendChild(name);

        var score = document.createElement("score");
        var scoreText = document.createTextNode(data[i].result.score.raw);
        score.appendChild(scoreText);
        row.appendChild(score); 

        board.appendChild(row);
    }
}

function previousPage() {
    if (page > 1) {
        page--;
    }
    var board = document.getElementById("leaderboard");
    while (board.firstChild) {
        board.removeChild(board.firstChild);
    }
    var maxRes = (numResults < (page * numPerPage)) ? numResults : (page * numPerPage);
    var minRes = (page * numPerPage) - numPerPage;
    for (var i = minRes; i < maxRes; i++) {
        var row = document.createElement("li");

        var num = document.createElement("num");
        var numText = document.createTextNode(i + 1);
        num.appendChild(numText);
        row.appendChild(num);

        var name = document.createElement("name");
        var nameText = document.createTextNode(data[i].actor.name);
        name.appendChild(nameText);
        row.appendChild(name);

        var score = document.createElement("score");
        var scoreText = document.createTextNode(data[i].result.score.raw);
        score.appendChild(scoreText);
        row.appendChild(score); 

        board.appendChild(row);
    }
}

function closeLeaderboard() {
    document.getElementById("leaderboardOverlay").style.height = "0%";
    var board = document.getElementById("leaderboard");
    while (board.firstChild) {
        board.removeChild(board.firstChild);
    }
    page = 1;
    pageMax = 0;
    numResults = 0;
}

function addLeaderboardEventListeners() {
    document.getElementById("obtn").addEventListener("click", openLeaderboard, false);
    document.getElementById("cbtn").addEventListener("click", closeLeaderboard, false);
}