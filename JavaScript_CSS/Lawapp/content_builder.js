function buildPage(data) {
    var html = "";

    html += setupPageType(data['type'], data['uniqueID']);
    data['content'].forEach(function (item) {
        html += setupContent(item);
    });

    html += "</div>"
    return html;
}

function setupPageType(type, id) {
    var html = "";
    html += "<div class=\"" + type + "\" id=\"" + id + "\"";
    if (id != "homepage") {
        html += " style=\"display: none;\"";
    }
    html += ">";
    return html;
}

function setupContent(item) {
    var type = item['type'];

    if (type == "h1") {
        return h1(item['text']);
    } else if (type == "button") {
        return button(item["text"], item["onclick"]["function"], item["onclick"]["params"]);
    }
}

function h1(text) {
    return "<h1>" + text + "</h1>";
}

function h2(text) {
    return "<h2>" + text + "</h2>";
}

function h3(text) {
    return "<h3>" + text + "</h3>";
}

function p(text) {
    return "<p>" + text + "</p>";
}

function button(text, func, params) {
    var paramString = "";
    params.forEach(function (param) {
        paramString += "'" + param + "', ";
    });
    paramString = paramString.slice(0, -2); // remove trailing comma and space
    return "<button class=\"button\" onclick=\"" + func + "(" + paramString + ")\">" + text + "</button>";
}