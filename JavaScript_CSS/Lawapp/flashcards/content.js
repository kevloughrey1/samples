var subtopics = {
    1: "Constitutional interpretation",
    2: "Institutions of the State",
    3: "Amending the constitution",
    4: "The separation of powers",
    5: "Referendum law",
    6: "International relations",
    7: "Obstacles to constitutional challenge",
    8: "General principles of constitutional rights",
    9: "Trial in due course of law",
    10: "Due process",
    11: "Freedom of expression",
    12: "Religion",
    13: "Equality",
    14: "Property rights",
    15: "Abortion and the unborn",
    16: "The right to die and refuse medical treatment",
    17: "The right to liberty",
    18: "Family and education",
    19: "Judicial review",
    20: "Social and fundamental rights: general principles"
}
var numSubtopics = 20;

var cases = [
    {
        "subtopic": "6",
        "casename": "Kavanagh v Governor of Mountjoy Prison",
        "year": "2002",
        "court": "IESC",
        "article": "29.5.2",
        "text": "International law (in this case International Covenant on Civil and Political Rights) is not part of domestic law unless legislated for by Oireachtas."
    },
    {
        "subtopic": "7",
        "casename": "Cahill v Sutton",
        "year": "1972",
        "court": "IESC",
        "text": "Locus Standi: Plaintiff must have an interest in case an argument being made. SC refused standing - plaintiff sought to vindicate the constitutional rights of a hypothetical third party."
    },
    {
        "subtopic": "8",
        "casename": "NVH v Minister for Justice",
        "year": "2017",
        "court": "IESC",
        "article": "40.1",
        "text": "Non-citizen rights: Rights may not always be the same."
    },
    {
        "subtopic": "1",
        "casename": "Quinn Supermarket v AG",
        "year": "1972",
        "court": "IR",
        "article": "44",
        "text": "Purposive approach: Jewish butcher allowed to open for an hour longer than non-Jewish butchers. Applicant alleged discrimination. Court found that while the Article 44.2.3 prohibited religious discrimination, the <b>purpose</b> of Article 44 as a while was to guarantee religious freedom. In this case the discrimination was necessary to uphold this freedom."
    },
    {
        "subtopic": "1",
        "casename": "DPP v O’Shea",
        "year": "1982",
        "court": "IESC",
        "text": "Harmonious approach: (Henchy J dissenting) \"Any single constitutional right or power is but a component in an ensemble of interconnected and interacting provisions which must be brought into play as part of a larger composition... It may be said of a Constitution, more than any other legal instrument, that the \"letter killers, but the spirit giveth life\" \"."
    },
    {
        "subtopic": "1",
        "casename": "Torrey v Ireland",
        "year": "1985",
        "court": "IESC",
        "text": "Harmonious approach: The Constitution must be read as a whole and not in isolation. Its provisions must be treated as interlocking parts of the general constitutional scheme."
    },
    {
        "subtopic": "1",
        "casename": "Sinnott v Minister for Education",
        "year": "2001",
        "court": "IESC",
        "article": "42",
        "text": "Historical approach: Court considered if the nature of primary education extended to a situation where it would be provided to a person with autism over the age of 18. While discussing various approaches, the court noted that one cannot divorce the present meaning of a provision entirely from its historical meaning."
    },
    {
        "subtopic": "1",
        "casename": "McGee v AG",
        "year": "1974",
        "court": "IR",
        "text": "Fundamental values and the present day: \"No interpretation of the Constitution is intended to be final for all time\". It is a living document."
    },
    {
        "subtopic": "1",
        "casename": "Zappone v Revenue Commissioners",
        "year": "2006",
        "court": "IEHC",
        "text": "Fundamental values and the present day: Court agreed that the Constitution is a living instrument (not one \"fixed in the permafrost of 1937\"), but that this approach did not apply when a new right is being asserted (e.g. same sex marriage). The historical approach was deemed more appropriate."
    },
    {
        "subtopic": "1",
        "casename": "Curtin v Dáil Éireann",
        "year": "2006",
        "court": "IESC",
        "text": "All approaches: Court considered the principles of constitutional interpretation. Summarised: correct balance must be struck between literal meaning of provisions and the Constitutional as a whole. Where words are not plain and unambiguous (literal approach), resort may be had to other parts of the Constitution. Historical context may be helpful, but that does not exclude contemporary interpretation."
    }
]