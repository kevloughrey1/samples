var b = 0;
var pageJSON = [];
var maxCount = 0;
var currCount = 0;

function loadTopicsPage() {
    var htmlString = "<div class=\"flashcard-page\"><ul>";
    htmlString += "<li><button class=\"link\" onclick=\"topicSelected('all')\">All</li>";
    for (var i = 0; i < numSubtopics; i++) {
        htmlString += "<li><button class=\"link\" onclick=\"topicSelected('" + (i + 1).toString() + "')\">" + subtopics[i + 1] + "</li>";
    }
    htmlString += "<li><button class=\"link\" onclick=\"home()\">Home</li>";
    htmlString += "</ul>";
    return htmlString;
}

function topicSelected(subtopic) {
    pageJSON = [];
    if (subtopic == "all") {
        pageJSON = cases;
    } else {
        for (var i = 0; i < cases.length; i++) {
            if (cases[i].subtopic === subtopic) {
                pageJSON.push(cases[i]);
            }
        }
    }
    maxCount = pageJSON.length;
    currCount = 0;

    buildPageSkeleton();
}

function buildPageSkeleton() {
    document.body.innerHTML = "";
    prevHtml = flashcardBuildPage(flashcard_content, "flashcard_prev");
    currHtml = flashcardBuildPage(flashcard_content, "flashcard_curr");
    nextHtml = flashcardBuildPage(flashcard_content, "flashcard_next");
    document.body.innerHTML += prevHtml + currHtml + nextHtml;
    document.body.innerHTML += "<div class=\"buttons\"><button id=\"prev\" class=\"link\" onclick=\"prev()\"><</button>&nbsp;<button id=\"home\" class=\"link\" onclick=\"fhome()\">Back</button>&nbsp;<button id=\"next\" class=\"link\" onclick=\"next()\">></button></div>";
    loadContent();
}

function next() {
    $('#flashcard_curr').hide('slide', {
        direction: 'left'
    }, 500);
    $('#flashcard_next').show('slide', {
        direction: 'right'
    }, 500, function() {
        currCount ++;
        loadContent();
    });
}

function prev() {
    $('#flashcard_prev').show('slide', {
        direction: 'left'
    }, 500);
    $('#flashcard_curr').hide('slide', {
        direction: 'right'
    }, 500, function() {
        currCount --;
        loadContent();
    });
}

function fhome() {
    document.body.innerHTML = loadTopicsPage();
}

function home() {
    document.body.innerHTML = "<div class=\"content\" id=\"app_content\"></div>";
    load();
}

function flashcardBuildPage(data, id) {
    var html = "";

    html += setupPageType(data['type'], id);
    data['content'].forEach(function (item) {
        html += setupContenta(item, id);
    });
    html += "</div>"
    return html;
}

function setupPageType(type, id) {
    var html = "";
    html += "<div class=\"" + type + "\" id=\"" + id + "\"";
    if (id != "flashcard_curr") {
        html += " style=\"display: none;\"";
    }
    html += ">";
    return html;
}

function setupContenta(item, id) {
    var type = item['type'];

    if (type == "h1") {
        return fh1(item['id'], id);
    } else if (type == "h2") {
        return fh2(item['id'], id);
    } else if (type == "h3") {
        return fh3(item['id'], id);
    } else if (type == "p") {
        return fp(item['id'], id);
    }
}

function fh1(uid, id) {
    return "<h1 id=\"" + id + "_" + uid + "\"></h1>";
}

function fh2(uid, id) {
    return "<h2 id=\"" + id + "_" + uid + "\"></h2>";
}

function fh3(uid, id) {
    return "<h3 id=\"" + id + "_" + uid + "\"></h3>";
}

function fp(uid, id) {
    return "<p id=\"" + id + "_" + uid + "\"></p>";
}

function loadContent() {
    if (maxCount == 0) {
        document.getElementById("flashcard_curr_header").innerHTML = "No cases found";
    } else {
        if (currCount > 0) {
            document.getElementById("prev").style.visibility = "initial";
            document.getElementById("flashcard_prev_header").innerHTML = pageJSON[currCount - 1].casename;
            document.getElementById("flashcard_prev_yearcourt").innerHTML = pageJSON[currCount - 1].year + ", " + pageJSON[currCount - 1].court;
            document.getElementById("flashcard_prev_text").innerHTML = pageJSON[currCount - 1].text;
        }
        else {
            document.getElementById("prev").style.visibility = "hidden";
        }
        document.getElementById("flashcard_curr_header").innerHTML = pageJSON[currCount].casename;
        document.getElementById("flashcard_curr_yearcourt").innerHTML = pageJSON[currCount].year + ", " + pageJSON[currCount].court;
        document.getElementById("flashcard_curr_text").innerHTML = pageJSON[currCount].text;

        if (currCount < maxCount - 1) {
            document.getElementById("next").style.visibility = "initial";
            document.getElementById("flashcard_next_header").innerHTML = pageJSON[currCount + 1].casename;
            document.getElementById("flashcard_next_yearcourt").innerHTML = pageJSON[currCount + 1].year + ", " + pageJSON[currCount + 1].court;
            document.getElementById("flashcard_next_text").innerHTML = pageJSON[currCount + 1].text;
        }
        else {
            document.getElementById("next").style.visibility = "hidden";
        }
    }
    document.getElementById("flashcard_curr").style.display = "initial";
    document.getElementById("flashcard_prev").style.display = "none";
    document.getElementById("flashcard_next").style.display = "none";
}