function buildPlugin(data) {
    var folder = data['content']['folder'];
    
   /* var head = document.head;
    var link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = folder + "/" + data['content']['cssfilename'] + ".css";
    head.appendChild(link);*/
/*
    for (var i = 0; i < data['content']['jsfilenames'].length; i++) {
        var script = document.createElement("script");
        script.src = folder + "/" + data['content']['jsfilenames'][i] + ".js";
        head.appendChild(script);
    }
*/
    var html = "";
    html += setupPluginPageType(data['content']['type'], data['uniqueID']);
    if (folder == "flashcards") {
        html += loadTopicsPage();
    }
    else {
        html += "<canvas id=\"myCanvas\" width=\"420px\" height=\"600px\"></canvas>";
    }
    return html;
}

function setupPluginPageType(type, id) {
    var html = "";
    html += "<div class=\"" + type + "\" id=\"" + id + "\" style=\"display: none;\">";
    return html;
}