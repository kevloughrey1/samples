var currentID = "homepage";

function test() {
    alert("Hello, World!");
}

function load() {
    var contentDiv = document.getElementById("app_content");
    content.forEach(function (pageData) {
        if (pageData['type'] != "plugin") {
            contentDiv.innerHTML += buildPage(pageData);
        } else {
            contentDiv.innerHTML += buildPlugin(pageData);
        }
    });
}

function goto(pageID) {
    $("#" + currentID).fadeOut("fast", function () {
        $("#" + pageID).fadeIn("fast", function () {
            currentID = pageID;
        });
    });
}