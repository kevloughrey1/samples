var content = [{
        "uniqueID": "homepage",
        "type": "menu-page",
        "content": [{
                "type": "h1",
                "text": "Constitutional Law",
            },
            {
                "type": "button",
                "text": "Flashcards",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "flashcardHome"
                    ]
                }
            },
            {
                "type": "button",
                "text": "Essays",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "essayHome"
                    ]
                }
            }
        ]
    },
    {
        "uniqueID": "flashcardHome",
        "type": "menu-page",
        "content": [{
                "type": "h1",
                "text": "Revision Flashcards",
            },
            {
                "type": "button",
                "text": "View Cards",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "flashcardPage"
                    ]
                }
            },
            {
                "type": "button",
                "text": "Revise",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "flashcardRevisePage"
                    ]
                }
            },
            {
                "type": "button",
                "text": "Home",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "homepage"
                    ]
                }
            }
        ]
    },
    {
        "uniqueID": "essayHome",
        "type": "menu-page",
        "content": [{
                "type": "h1",
                "text": "Essay Placeholder",
            },
            {
                "type": "button",
                "text": "Home",
                "onclick": {
                    "function": "goto",
                    "params": [
                        "homepage"
                    ]
                }
            }
        ]
    },
    {
        "uniqueID": "flashcardPage",
        "type": "plugin",
        "content": {
            "type": "flashcards",
            "folder": "flashcards",
            "cssfilename": "flashcards", 
            "jsfilenames": ["flashcards", "content", "layout"]
        }
    },
    {
        "uniqueID": "flashcardRevisePage",
        "type": "plugin",
        "content": {
            "type": "matching",
            "folder": "matching",
            "cssfilename": "na", 
            "jsfilenames": ["flashcards", "content", "layout"]
        }
    }
]