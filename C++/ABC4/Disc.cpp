#include "Disc.h"

Disc::Disc(Vector2& position, float radius, PhysicsWorld* world)
{
	m_position = position;
	m_radius = radius;
	m_physicsObject = world->AddObject(m_position, m_radius, true);
}

Disc::~Disc()
{
}

void Disc::Update()
{
	m_position = m_physicsObject->GetPosition();
}