#ifndef DISC_H
#define DISC_H

#include "RenderableObject.h"
#include "PhysicsWorld.h"

class Disc : public RenderableObject
{
public:
	Disc() {}
	Disc(Vector2& position, float radius, PhysicsWorld* world);
	~Disc();
	void Update();
private:
	PhysicsObject* m_physicsObject;
	float m_radius;
};

#endif