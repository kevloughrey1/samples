#ifndef PHYSICSWORLD_H
#define PHYSICSWORLD_H

#include "PhysicsObject.h"
#include <vector>

class PhysicsWorld
{
public:
	PhysicsWorld(float gravityScale);
	~PhysicsWorld();
	PhysicsObject* AddObject(Vector2& position, float radius, bool dynamic);
	void Step(float timeStep);
private:
	float m_gravityScale;
	std::vector<PhysicsObject*> m_objects;
};

#endif