#ifndef VECTOR2_H
#define VECTOR2_H

#include <iostream>
#include <cmath>

struct Vector2
{
	float m_x;
	float m_y;

	Vector2(float x = 0.0f, float y = 0.0f) : m_x(x), m_y(y) {}
	~Vector2() {}

	inline void Set(float x, float y)
	{
		m_x = x;
		m_y = y;
	}

	inline float Length()
	{
		return sqrtf((m_x * m_x) + (m_y * m_y));
	}

	inline float LengthSquared()
	{
		return (m_x * m_x) + (m_y * m_y);
	}

	inline float DistanceTo(Vector2& v)
	{
		return sqrtf(((m_x - v.m_x) * (m_x - v.m_x)) + ((m_y - v.m_y) * (m_y - v.m_y)));
	}

	inline Vector2& Normalize()
	{
		float length = Length();
		if (length != 0)
		{
			m_x /= length; m_y /= length;
			return *this;
		}
		return *this;
	}

	inline Vector2 NormalizedCopy()
	{
		float lengthSquared = LengthSquared();
		if (lengthSquared != 0)
		{
			return Vector2(m_x / lengthSquared, m_y / lengthSquared);
		}
		return Vector2(0, 0);
	}

	inline Vector2&	operator=(Vector2& v)
	{
		m_x = v.m_x;
		m_y = v.m_y;
		return *this;
	}

	inline Vector2&	operator+=(Vector2& v)
	{
		m_x += v.m_x;
		m_y += v.m_y;
		return *this;
	}

	inline Vector2&	operator-=(Vector2& v)
	{
		m_x -= v.m_x;
		m_y -= v.m_y;
		return *this;
	}

	inline Vector2& operator*=(float f)
	{
		m_x *= f;
		m_y *= f;
		return *this;
	}

	inline Vector2& operator/=(float f)
	{
		m_x /= f;
		m_y /= f;
		return *this;
	}

	inline Vector2 operator+(Vector2& v)
	{
		return Vector2(m_x + v.m_x, m_y + v.m_y);
	}

	inline Vector2 operator-(Vector2& v)
	{
		return Vector2(m_x - v.m_x, m_y - v.m_y);
	}

	inline Vector2 operator*(Vector2& v)
	{
		return Vector2(m_x * v.m_x, m_y * v.m_y);
	}

	inline Vector2 operator*(float f)
	{
		return Vector2(m_x * f, m_y * f);
	}

	inline Vector2 operator/(float f)
	{
		return Vector2(m_x / f, m_y / f);
	}

	inline bool operator==(Vector2& v)
	{
		return (m_x == v.m_x) && (m_y == v.m_y);
	}

	inline bool operator!=(Vector2& v)
	{
		return (m_x != v.m_x) || (m_y != v.m_y);
	}

	friend std::ostream& operator<<(std::ostream& os, Vector2& m)
	{
		return os << "(" << m.m_x << ", " << m.m_y << ")" << std::endl;
	}
};

#endif