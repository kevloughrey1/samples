#ifndef DEBUGDRAWER_H
#define DEBUGDRAWER_H

#include "Vector2.h"
#include "RGBAColour.h"
#include "Renderer.h"

inline void DrawLine(Vector2& start, Vector2& end, RGBAColour& renderColour, Renderer* renderer)
{
	renderer->DrawLine(static_cast<int>(start.m_x), static_cast<int>(start.m_y), static_cast<int>(end.m_x), static_cast<int>(end.m_y), renderColour);
}

inline void DrawRectangle(Vector2& topLeft, int width, int height, RGBAColour& renderColour, Renderer* renderer)
{
	renderer->DrawLine(static_cast<int>(topLeft.m_x), static_cast<int>(topLeft.m_y), static_cast<int>(topLeft.m_x) + width, static_cast<int>(topLeft.m_y), renderColour);
	renderer->DrawLine(static_cast<int>(topLeft.m_x), static_cast<int>(topLeft.m_y), static_cast<int>(topLeft.m_x), static_cast<int>(topLeft.m_y) + height, renderColour);
	renderer->DrawLine(static_cast<int>(topLeft.m_x) + width, static_cast<int>(topLeft.m_y), static_cast<int>(topLeft.m_x) + width, static_cast<int>(topLeft.m_y) + height, renderColour);
	renderer->DrawLine(static_cast<int>(topLeft.m_x), static_cast<int>(topLeft.m_y) + height, static_cast<int>(topLeft.m_x) + width, static_cast<int>(topLeft.m_y) + height, renderColour);
}

inline void DrawRectangle(Vector2& topLeft, Vector2& bottomRight, RGBAColour& renderColour, Renderer* renderer)
{
	int width = static_cast<int>(bottomRight.m_x) - static_cast<int>(topLeft.m_x);
	int height = static_cast<int>(bottomRight.m_y) - static_cast<int>(topLeft.m_y);
	DrawRectangle(topLeft, width, height, renderColour, renderer);
}

inline void DrawCircle(Vector2& centre, float radius, RGBAColour& color, uint8_t sides, Renderer* renderer)
{
	double sideAngle = (2.0 * M_PI) / sides;
	float overallAngle = static_cast<float>(sideAngle);

	Vector2 start, end;
	end.Set(radius, 0.0f);
	end += centre;

	for (int i = 0; i < sides; ++i)
	{
		start = end;
		end.Set(cosf(overallAngle) * radius, sinf(overallAngle) * radius);
		end += centre;
		overallAngle += static_cast<float>(sideAngle);
		DrawLine(start, end, color, renderer);
	}
}

#endif