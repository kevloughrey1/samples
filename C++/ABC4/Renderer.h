#ifndef RENDERER_H
#define RENDERER_H

#include <SDL.h>
#include "RGBAColour.h"

struct Window
{
	SDL_Window* m_window = NULL;
	Window()
	{
		SDL_Init(SDL_INIT_EVERYTHING);
		m_window = SDL_CreateWindow("Cn4 Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, SDL_WINDOW_SHOWN);
	}
	~Window() { SDL_DestroyWindow(m_window); }
};

struct Renderer
{
	SDL_Renderer* m_renderer = NULL;
	Renderer(Window* window)
	{
		m_renderer = SDL_CreateRenderer(window->m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		SDL_SetRenderDrawColor(m_renderer, 100, 149, 237, 255);
		SDL_UpdateWindowSurface(window->m_window);
	}

	~Renderer() { SDL_DestroyRenderer(m_renderer); }

	inline void ClearRenderer()
	{
		SDL_RenderClear(m_renderer);
	}

	inline void RenderPresent()
	{
		SDL_RenderPresent(m_renderer);
	}

	inline void GetDrawColour(uint8_t& r, uint8_t& g, uint8_t& b, uint8_t& a)
	{
		SDL_GetRenderDrawColor(m_renderer, &r, &g, &b, &a);
	}

	inline void SetDrawColour(RGBAColour& c)
	{
		SDL_SetRenderDrawColor(m_renderer, c.m_r, c.m_g, c.m_b, c.m_a);
	}

	inline void DrawLine(int x1, int y1, int x2, int y2, RGBAColour& c)
	{
		uint8_t r, g, b, a;
		GetDrawColour(r, g, b, a);
		SetDrawColour(c);
		SDL_RenderDrawLine(m_renderer, x1, y1, x2, y2);
		SetDrawColour(RGBAColour(r, g, b, a));
	}
};

#endif