#include "UtilityFunctions.h"

namespace GameUtilities
{
	float ToMetres(float value)
	{
		return value / pixelMetreScale;
	}

	float ToPixels(float value)
	{
		return value * pixelMetreScale;
	}
}