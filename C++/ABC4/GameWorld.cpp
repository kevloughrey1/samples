#include "GameWorld.h"

GameWorld::GameWorld()
{
	m_physicsWorld = new PhysicsWorld(1.0f);
	m_disc = Disc(Vector2(10.0f, 20.0f), 1.0f, m_physicsWorld);
}

GameWorld::~GameWorld()
{
	delete m_physicsWorld;
}

void GameWorld::Update()
{
	m_physicsWorld->Step(GameUtilities::timeStep);
	m_disc.Update();
}

void GameWorld::Render(Renderer* renderer)
{
	m_disc.Render(renderer);
}