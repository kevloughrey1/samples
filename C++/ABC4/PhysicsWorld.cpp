#include "PhysicsWorld.h"

PhysicsWorld::PhysicsWorld(float gravityScale)
	: m_gravityScale(gravityScale)
{
}

PhysicsWorld::~PhysicsWorld()
{
	for (size_t i = 0; i < m_objects.size(); i++)
	{
		delete m_objects.at(i);
	}
}

PhysicsObject* PhysicsWorld::AddObject(Vector2& position, float radius, bool dynamic)
{
	PhysicsObject* p = new PhysicsObject(position, radius, dynamic);
	m_objects.push_back(p);
	return m_objects.back();
}

void PhysicsWorld::Step(float timeStep)
{
	for (size_t i = 0; i < m_objects.size(); ++i)
	{
		if (m_objects.at(i)->GetType() == PhysicsObject::CIRCLE
			&& m_objects.at(i)->IsDynamic()) 
		{
			m_objects.at(i)->Update(timeStep, m_gravityScale);
		}
	}
}