#ifndef GAMEWORLD_H
#define GAMEWORLD_H

#include "Disc.h"
#include "PhysicsWorld.h"

class GameWorld
{
public:
	GameWorld();
	~GameWorld();
	void Update();
	void Render(Renderer* renderer);

private:
	Disc m_disc;
	PhysicsWorld* m_physicsWorld;
};

#endif