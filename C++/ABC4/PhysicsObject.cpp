#include "PhysicsObject.h"

PhysicsObject::PhysicsObject(Vector2& position, float radius, bool dynamic)
	: m_position(position), m_initialPosition(position), m_radius(radius), m_dynamic(dynamic), m_activeTimeSteps(0)
{
	m_shape = PhysicsObject::CIRCLE;
	m_velocity = Vector2(5.0f, -20.0f);
	m_initialVelocity = m_velocity;
}

PhysicsObject::PhysicsObject(Vector2& centre, float width, float height)
	: m_position(centre), m_width(width), m_height(height)
{
	m_shape = PhysicsObject::RECTANGLE;
	m_dynamic = false;
}

PhysicsObject::shape PhysicsObject::GetType()
{
	return m_shape;
}

bool PhysicsObject::IsDynamic()
{
	return m_dynamic;
}

void PhysicsObject::Update(float timeStep, float gravityScale)
{
	++m_activeTimeSteps;
	float t = m_activeTimeSteps * timeStep;
	float g = GameUtilities::gravity * gravityScale;
	m_position.m_x = m_initialVelocity.m_x * t;
	m_position.m_y = (m_initialVelocity.m_y * t) - (0.5 * g * t * t);
}

Vector2 PhysicsObject::GetPosition()
{
	return m_position;
}