#ifndef PHYSICSOBJECT_H
#define PHYSICSOBJECT_H

#include "Vector2.h"
#include "UtilityFunctions.h"

class PhysicsObject
{
public:
	PhysicsObject() {}
	PhysicsObject(Vector2& position, float radius, bool dynamic);
	PhysicsObject(Vector2& centre, float width, float height);
	~PhysicsObject() {}
	enum shape { CIRCLE, RECTANGLE };
	void Update(float timeStep, float gravityScale);
	PhysicsObject::shape GetType();
	bool IsDynamic();
	Vector2 GetPosition();
private:
	Vector2 m_position;
	Vector2 m_velocity;
	Vector2 m_initialPosition;
	Vector2 m_initialVelocity;
	float m_activeTimeSteps;
	float m_radius;
	float m_width;
	float m_height;
	float m_dynamic;
	PhysicsObject::shape m_shape;
};

#endif