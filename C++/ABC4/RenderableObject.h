#ifndef RENDERABLEONJECT_H
#define RENDERABLEOBJECT_H

#include "DebugDrawer.h"
#include "UtilityFunctions.h"

class RenderableObject 
{
public:
	void Render(Renderer* renderer)
	{
		
		Vector2 pos(GameUtilities::ToPixels(m_position.m_x), GameUtilities::ToPixels(m_position.m_y));
		float rad = GameUtilities::ToPixels(2.0f);
		DrawCircle(pos, rad, RGBAColour(255, 0, 0, 1), 36, renderer);
	}
protected:
	Vector2 m_position;
};

#endif