#include "Renderer.h"
#include "Game.h"
#include "UtilityFunctions.h"

#include <time.h> 


int main(int argc, char* args[])
{
	unsigned int a = 0;
	unsigned int b = 0;
	double updateTime = 0;
	const float targetFPS = GameUtilities::targetFPS;
	const float desiredFrameTime = 1000.0f / targetFPS;
	int countedFrames = 0;
	Window* m_w = new Window();
	Renderer* renderer = new Renderer(m_w);

	Game* m_game = new Game();

	while (true)
	{
		a = SDL_GetTicks();
		updateTime = a - b;

		if (updateTime < desiredFrameTime)
		{
			SDL_Delay(desiredFrameTime - updateTime);
		}

		++countedFrames;
		b = a;
		m_game->Update();
		renderer->ClearRenderer();
		m_game->Render(renderer);
		renderer->RenderPresent();

/*		clock_t t = clock();
		float t1 = (float)t;
		float avgFPS = countedFrames / (t1 / CLOCKS_PER_SEC);
		std::cout << avgFPS << std::endl;*/
	}

	delete m_w;
	delete renderer;
	delete m_game;

	SDL_Quit();
	return 0;
}