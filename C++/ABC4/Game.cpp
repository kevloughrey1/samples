#include "Game.h"

Game::Game()
	: m_world(new GameWorld()) 
{

}

Game::~Game()
{
	delete m_world;
}

void Game::Update()
{
	m_world->Update();
}

void Game::Render(Renderer* renderer)
{
	m_world->Render(renderer);
}