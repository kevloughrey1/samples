#ifndef GAME_H
#define GAME_H

#include "GameWorld.h"

class Game 
{
public:
	Game();
	~Game();
	void Update();
	void Render(Renderer* renderer);

private:
	GameWorld* m_world;
};

#endif