#ifndef UTILITYFUNCTIONS_H
#define UTILITYFUNCTIONS_H

namespace GameUtilities
{
	float ToMetres(float value);
	float ToPixels(float value);

	const float pixelMetreScale = 15.0f;
	const float targetFPS = 60.0f;
	const float timeStep = 1 / targetFPS;
	const float gravity = -9.81f;
}

#endif