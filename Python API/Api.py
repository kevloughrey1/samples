import re, csv, xml.dom.minidom, smtplib, os, binascii
from functools import wraps
from flask import Flask, render_template, request, redirect, url_for, session, Response
from passlib.hash import pbkdf2_sha512
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
import traceback

webapp = Flask(__name__)
webapp.secret_key = b'utghvqH%U$9|6I@V9Ph0?R0]&C.Ta4M6r4G1_'
d_host='***.tcd.ie'
d_user='***'
d_password='***'
d_database='decide'
m_ip='***.tcd.ie'
m_sheet_url='http://***.tcd.ie/%s.pdf'

def send_mail(to, subject, mesg):
    m_from = '***@gmail.com'
    m_to = to
    msg = MIMEMultipart()
    msg['From'] = m_from
    msg['To'] = m_to
    msg['Subject'] = subject
    message = mesg
    msg.attach(MIMEText(message))
    mailserver = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    mailserver.login(m_from, '***')
    mailserver.ehlo()
    mailserver.sendmail(m_from,m_to,msg.as_string())
    mailserver.close()   

def import_mapping_dictionary(path, dict):
    reader = csv.DictReader(open(path))
    for row in reader:
        for column, value in row.items():
            if value != '':
                dict.setdefault(column, []).append(value)
    return dict
    
glycemicDict = {}
glycemicDict = import_mapping_dictionary('glyDict.csv', glycemicDict)

htnDict = {}
htnDict = import_mapping_dictionary('htnDict.csv', htnDict)
        
lipidDict = {}
lipidDict = import_mapping_dictionary('lipidDict.csv', lipidDict)

idList = list()
idListSpace = list()

def concatenate_list(list):
    list_concatenated = ""
    for l in list:
        list_concatenated += l + '&'
    list_concatenated = list_concatenated[:-1]
    return list_concatenated

def regex_replace(nodes, text):
    regex = re.compile('{{.*?}}')
    matches = regex.findall(nodes[0])
    
    temps_list = text.split("&")
    for val in matches:
        if "&" not in val:
            val1 = val.replace('{', '')
            val1 = val1.replace('}', '')
            newval = int(val1) - 1
            nodes = [w.replace(val, temps_list[newval]) for w in nodes]
    return nodes
        
def parse_search(q, dict):
    search_key = ""
    meds_list = q.split("&")
    for med in meds_list:
        search_key += get_group(dict, med) + "&"
    search_key = search_key[:-1]

    search_list = search_key.split("&")
    search_list.sort()
    search_key = ""
    for s in search_list:
        search_key += s + "&"
    search_key = search_key[:-1]
    return search_key

def get_group(list, medication):
    for group, meds in list.items():
    
        for med in meds:
            if med.lower() == medication.lower():
                return group
    return "!Invalid!"

def parse_search_alternate(q, dict):
    search_key = ""
    meds_list = q.split("&")
    for med in meds_list:
        search_key += get_group_alternate(dict, med) + "&"
    search_key = search_key[:-1]

    search_list = search_key.split("&")
    search_list.sort()
    search_key = ""
    for s in search_list:
        search_key += s + "&"
    search_key = search_key[:-1]
    return search_key
    
def get_group_alternate(list, medication):
    for group, meds in list.items():
    
        for med in meds:
            if med.lower() == medication.lower():
                return group
    return medication  
    
def decide_glycemic(results_list, search_terms, mapping_list, hba):
    search_terms = parse_search(search_terms, mapping_list)
    if "!Invalid!" in search_terms:
        return "<t2dm><t2dmError>An invalid glycemic medication was input.</t2dmError></t2dm>"
    for group, decision in results_list.items():
        if group.lower() == search_terms.lower():
            if hba >= 80:
                return decision[1]
            else:
                return decision[0]
    return "<t2dm><t2dmError>Glycemic decision not found. Valid medications were input, but a decision was not found for this combination.</t2dmError></t2dm>"  

def decide_htn(results_list, search_terms, mapping_list, systolic, diastolic, primary):
    search_terms = parse_search(search_terms, mapping_list)
    if "!Invalid!" in search_terms:
        return "<htn><htnError>An invalid HTN medication was input.</htnError></htn>"
    for group, decision in results_list.items():
        if group.lower() == search_terms.lower():
            if primary.lower() == "true":
                if diastolic >= 85 or systolic >= 140:
                    return decision[1]
                else:
                    return decision[0]
            else:
                if diastolic >= 80 or systolic >= 130:
                    return decision[1]
                else:
                    return decision[0]
    return "<htn><htnError>HTN decision not found. Valid medications were input, but a decision was not found for this combination.</htnError></htn>"      

def decide_lipid(results_list, search_terms, mapping_list, qrisk, primary):
    search_terms = parse_search(search_terms, mapping_list)
    
    if "!Invalid!" in search_terms:
        return "<lipid><lipidError>An invalid lipid medication was input.</lipidError></lipid>"
    for group, decision in results_list.items():
        if group.lower() == search_terms.lower():
            if primary.lower() == "true":
                if qrisk >= 10:
                    return decision[1]
                else:
                    return decision[0]
            else:
                if qrisk >= 10:
                    return decision[2]
                else:
                    return decision[0]
    return "<lipid><lipidError>Lipid decision not found. Valid medications were input, but a decision was not found for this combination.</lipidError></lipid>"       
    
def xmlify(tag, data):
    return "<" + tag + ">" + data + "</" + tag + ">"
    
glycemic_dict = {}

with open('HBATemplateText.csv', 'rt') as f:
    reader = csv.reader(f)
    node_list = list(reader)
    blank_node_list = node_list[:]

with open('HBAVariableText.csv', 'rt') as f:
    reader = csv.reader(f)
    text_list = list(reader)

for i in range (0, len(text_list[0])):
    key = text_list[8][i]
    key_list = key.split("&")
    key_list.sort()
    key = ""
    for k in key_list:
        key += k + "&"
    key = key[:-1]
    
    patient_not_included_text = node_list[1][0]
    patient_not_included_text = xmlify("t2dm", xmlify("t2dmNotIncluded", patient_not_included_text))
    
    node_list[3] = regex_replace(node_list[3], text_list[3][i])
    included_text_one = node_list[3][0]
    
    node_list[4] = regex_replace(node_list[4], text_list[4][i])
    included_text_two = node_list[4][0]
    
    included_text_three = ""

    options_list = text_list[5][i].split("&")
    for opt in options_list:
        txt = regex_replace(node_list[5], opt)
        pos = 0
        if (txt[0].replace(' ','') not in idList):
            idList.append(txt[0].replace(' ',''))
            idListSpace.append(txt[0])
            pos = len(idList) - 1
        else:
            pos = idList.index(txt[0].replace(' ',''))
        the_text = xmlify("id", str(pos)) + xmlify("text", txt[0]) + xmlify("sheet", m_sheet_url % opt.lower().replace(' ',''))
        included_text_three += xmlify("t2dmIncreaseDoseElement", the_text)
    
    included_text_four = node_list[6][0]
    
    included_text_five = ""
    options_list = text_list[6][i].split("&")
    for opt in options_list:
        pos = 0
        if (opt.replace(' ','') not in idList):
            idList.append(opt.replace(' ',''))
            idListSpace.append(opt)
            pos = len(idList) - 1
        else:
            pos = idList.index(opt.replace(' ',''))
        the_text = xmlify("id", str(pos)) + xmlify("text", opt)
        included_text_five += xmlify("t2dmChangeMedsElement", the_text)
    
    node_list[7] = regex_replace(node_list[7], text_list[7][i])
    pos = 0
    if (node_list[7][0].replace(' ','') not in idList):
        idList.append(node_list[7][0].replace(' ',''))
        idListSpace.append(node_list[7][0])
        pos = len(idList) - 1
    else:
        pos = idList.index(node_list[7][0].replace(' ',''))
    the_text = xmlify("id", str(pos)) + xmlify("text", node_list[7][0])
    included_text_six = the_text
        
    decision_text = ("<t2dm>" +
                     xmlify("t2dmHeader", included_text_one) + 
                     xmlify("t2dmIncreaseDose", included_text_two) + 
                     xmlify("t2dmIncreaseDoseElements", included_text_three) + 
                     xmlify("t2dmChangeMeds", included_text_four) + 
                     xmlify("t2dmChangeMedsElements", included_text_five) + 
                     xmlify("t2dmNoChange", included_text_six) +
                     "</t2dm>")
    
    node_list = blank_node_list[:]
    
    glycemic_dict[key] = [patient_not_included_text, decision_text]

node_list[:] = []
text_list[:] = []

htn_dict = {}

with open('BPTemplateText.csv', 'rt') as f:
    reader = csv.reader(f)
    node_list = list(reader)
    blank_node_list = node_list[:]

with open('BPVariableText.csv', 'rt') as f:
    reader = csv.reader(f)
    text_list = list(reader)

for i in range (0, len(text_list[0])):
    key = text_list[6][i]
    key_list = key.split("&")
    key_list.sort()
    key = ""
    for k in key_list:
        key += k + "&"
    key = key[:-1]
    
    patient_not_included_text = regex_replace(node_list[0], text_list[0][i])[0]
    patient_not_included_text = xmlify("htn", xmlify("htnNoAction", patient_not_included_text))
    
    included_text_one = regex_replace(node_list[1], text_list[1][i])[0]
   
    included_text_two = regex_replace(node_list[2], text_list[2][i])[0]
 
    included_text_three = ""
    options_list = text_list[3][i].split("&")
    for opt in options_list:
        pos = 0
        txt = regex_replace(node_list[3], opt)[0]
        if (txt.replace(' ','') not in idList):
            idList.append(txt.replace(' ',''))
            idListSpace.append(txt)
            pos = len(idList) - 1
        else:
            pos = idList.index(txt.replace(' ',''))
        the_text = xmlify("id", str(pos)) + xmlify("text", txt)
        included_text_three += xmlify("htnIncreaseDoseElement", the_text)
    
    included_text_four = node_list[4][0]
    
    included_text_five = ""  
    options_list = text_list[4][i].split("&")
    for opt in options_list:
        pos = 0
        if (opt.replace(' ','') not in idList):
            idList.append(opt.replace(' ',''))
            idListSpace.append(opt)
            pos = len(idList) - 1
        else:
            pos = idList.index(opt.replace(' ',''))
        the_text = xmlify("id", str(pos)) + xmlify("text", opt)
        included_text_five += xmlify("htnChangeMedsElement", the_text)
    
    included_text_six = regex_replace(node_list[5], text_list[5][i])[0]
    pos = 0
    if (included_text_six.replace(' ','') not in idList):
        idList.append(included_text_six.replace(' ',''))
        idListSpace.append(included_text_six)
        pos = len(idList) - 1
    else:
        pos = idList.index(included_text_six.replace(' ',''))
    the_text = xmlify("id", str(pos)) + xmlify("text", included_text_six)
    included_text_six = the_text
    
    decision_text = ("<htn>" +
                 xmlify("htnHeader", included_text_one) + 
                 xmlify("htnIncreaseDose", included_text_two) + 
                 xmlify("htnIncreaseDoseElements", included_text_three) + 
                 xmlify("htnChangeMeds", included_text_four) + 
                 xmlify("htnChangeMedsElements", included_text_five) + 
                 xmlify("htnNoChange", included_text_six) +
                 "</htn>")
    
    node_list = blank_node_list[:]
    
    htn_dict[key] = [patient_not_included_text, decision_text]

node_list[:] = []
text_list[:] = []

lipid_dict = {}

with open('LipidTemplateText.csv', 'rt') as f:
    reader = csv.reader(f)
    node_list = list(reader)
    blank_node_list = node_list[:]

with open('LipidVariableText.csv', 'rt') as f:
    reader = csv.reader(f)
    text_list = list(reader)

i = 0;
while i < len(text_list[0]):
    if text_list[0][i] != "":
        key = text_list[6][i]
        key_list = key.split("&")
        key_list.sort()
        key = ""
        for k in key_list:
            key += k + "&"
        key = key[:-1]
        
        patient_not_included_text = regex_replace(node_list[0], text_list[0][i])[0]
        patient_not_included_text = xmlify("lipid", xmlify("lipidNoAction", patient_not_included_text))
        primary_included_text_one = ""
        primary_included_text_one = regex_replace(node_list[1], text_list[1][i])[0]
        primary_included_text_two = ""
        options_list = text_list[3][i].split("&")
        for opt in options_list:
            pos = 0
            if (opt.replace(' ','') not in idList):
                idList.append(opt.replace(' ',''))
                idListSpace.append(opt)
                pos = len(idList) - 1
            else:
                pos = idList.index(opt.replace(' ',''))
            the_text = xmlify("id", str(pos)) + xmlify("text", opt)
            primary_included_text_two += xmlify("lipidIncreaseDoseElement", the_text)
            primary_included_text_two = xmlify("lipidIncreaseDoseElements", primary_included_text_two)
        primary_included_text_three = ""
        options_list = text_list[4][i].split("&")
        for opt in options_list:
            pos = 0
            if (opt.replace(' ','') not in idList):
                idList.append(opt.replace(' ',''))
                idListSpace.append(opt)
                pos = len(idList) - 1
            else:
                pos = idList.index(opt.replace(' ',''))
            the_text = xmlify("id", str(pos)) + xmlify("text", opt)
            primary_included_text_three += xmlify("lipidChangeMedsElement", the_text)
            primary_included_text_three = xmlify("lipidChangeMedsElements", primary_included_text_three)
        pos = 0
        primary_included_text_four = regex_replace(node_list[5], text_list[5][i+1])[0]
        if (primary_included_text_four.replace(' ','') not in idList):
            idList.append(primary_included_text_four.replace(' ',''))
            idListSpace.append(primary_included_text_four)
            pos = len(idList) - 1
        else:
            pos = idList.index(primary_included_text_four.replace(' ',''))
        the_text = xmlify("id", str(pos)) + xmlify("text", primary_included_text_four)
        primary_included_text_four = the_text
        primary_decision_text = ("<lipid>" +
             xmlify("lipidHeader", primary_included_text_one) + 
             xmlify("lipidIncreaseDose", "1 Intensify Treatment: Increase dose of existing medication if possible") + 
             primary_included_text_two +
             xmlify("lipidChangeMeds", "2 Intensification: Change existing medications e.g. add an extra medication or switch drugs if they have not been effective") +
             primary_included_text_three +
             xmlify("lipidNoChange", primary_included_text_four) +
             "</lipid>")
        
        secondary_included_text_one = regex_replace(node_list[2], text_list[2][i+1])[0]
        secondary_included_text_two = ""
        options_list = text_list[3][i+1].split("&")
        for opt in options_list:
            pos = 0
            if (opt.replace(' ','') not in idList):
                idList.append(opt.replace(' ',''))
                idListSpace.append(opt)
                pos = len(idList) - 1
            else:
                pos = idList.index(opt.replace(' ',''))
            the_text = xmlify("id", str(pos)) + xmlify("text", opt)
            secondary_included_text_two += xmlify("lipidIncreaseDoseElement", the_text)
            secondary_included_text_two = xmlify("lipidIncreaseDoseElements", secondary_included_text_two)
        secondary_included_text_three = ""
        options_list = text_list[4][i+1].split("&")
        for opt in options_list:
            pos = 0
            if (opt.replace(' ','') not in idList):
                idList.append(opt.replace(' ',''))
                idListSpace.append(opt)
                pos = len(idList) - 1
            else:
                pos = idList.index(opt.replace(' ',''))
            the_text = xmlify("id", str(pos)) + xmlify("text", opt)
            secondary_included_text_three += xmlify("lipidChangeMedsElement", the_text)
            secondary_included_text_three = xmlify("lipidChangeMedsElements", secondary_included_text_three)
        pos = 0
        secondary_included_text_four = regex_replace(node_list[5], text_list[5][i+1])[0]
        if (secondary_included_text_four.replace(' ','') not in idList):
            idList.append(secondary_included_text_four.replace(' ',''))
            idListSpace.append(secondary_included_text_four)
            pos = len(idList) - 1
        else:
            pos = idList.index(secondary_included_text_four.replace(' ',''))
            the_text = xmlify("id", str(pos)) + xmlify("text", secondary_included_text_four)
            secondary_included_text_four = the_text
        secondary_decision_text = ("<lipid>" +
             xmlify("lipidHeader", secondary_included_text_one) + 
             xmlify("lipidIncreaseDose", "1 Intensify Treatment: Increase dose of existing medication if possible") + 
             secondary_included_text_two +
             xmlify("lipidChangeMeds", "2 Intensification: Change existing medications e.g. add an extra medication or switch drugs if they have not been effective") +
             secondary_included_text_three +
             xmlify("lipidNoChange", "3 You do not choose to intensify medication(s)") +
             secondary_included_text_four +
             "</lipid>")
             
        node_list = blank_node_list[:]
        lipid_dict[key] = [patient_not_included_text, primary_decision_text, secondary_decision_text]
        
    i+=2
        
import mysql.connector
conn = mysql.connector.connect(host=d_host,
                   user=d_user, 
                   password=d_password, 
                   database=d_database, )
cursor = conn.cursor()
for idE in idListSpace: 
    idE = idE.replace('\'', '\\\'')
    idE = idE.replace(r'"', r'\"')
    SQL = """INSERT INTO decision_ids (text) 
             SELECT '""" + str(idE) + """' FROM dual 
             WHERE NOT EXISTS (SELECT * FROM decision_ids
             WHERE text='""" + str(idE) + """') 
             LIMIT 1 """
    cursor.execute( SQL )
    conn.commit()
conn.close()
cursor.close()

def check_logged_in(func):
    """ This decorator function checks to see that a valid user is 
        logged in. If yes, run the called function. If not, send
        the user to the login page. 
    """
    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if 'logged-in' in session:
            return(func(*args, **kwargs))
        else:
            return(func(*args, **kwargs)) #return Response(xmlify("loginStatus", "You are not logged in"), mimetype='text/xml')
    return wrapped_function        
 
@webapp.route('/query')
@check_logged_in
def decide():
    glycelmic_meds_list = request.args.getlist('glyMed')
    glycelmic_meds_list = concatenate_list(glycelmic_meds_list)
    htn_meds_list = request.args.getlist('htnMed')
    htn_meds_list = concatenate_list(htn_meds_list)
    lipid_meds_list = request.args.getlist('lipidMed')
    lipid_meds_list = concatenate_list(lipid_meds_list)
    
    hba1c = request.args.get('hba1c')
    patientID = request.args.get('patientID')
    gpID = request.args.get('gpID')
    cholest = request.args.get('cholesterol')
    systolic = int(request.args.get('syst'))
    diastolic = int(request.args.get('diast'))
    primary = request.args.get('primary')
    qrisk = int(request.args.get('qrisk'))
    
    final_bp = str(systolic) + "/" + str(diastolic)
    prevention = "Primary" if primary.lower() == "true" else "Secondary"
    
    search_terms = parse_search_alternate(glycelmic_meds_list, glycemicDict)
    gly_meds = search_terms
    
    search_terms = parse_search_alternate(htn_meds_list, htnDict)
    htn_meds = search_terms
            
    search_terms = parse_search_alternate(lipid_meds_list, lipidDict)
    lipid_meds = search_terms
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    now = time.strftime('%Y-%m-%d %H:%M:%S')
    SQL = """insert into transactions SET Date='""" + str(now) +"""', ID_patient='""" + str(patientID) + """', ID_GP='""" + str(gpID) + """'"""
    cursor.execute( SQL )
    SQL = """SELECT MAX( ID_Transaction ) FROM transactions"""
    cursor.execute(SQL)
    transaction_id = cursor.fetchone()
    try:
        transaction_id = transaction_id[0]
    except:
        return None
    SQL = """insert into gly_decision SET ID_Transaction='""" + str(transaction_id) + """'"""
    cursor.execute( SQL )
    SQL = """insert into htn_decision SET ID_Transaction='""" + str(transaction_id) + """'"""
    cursor.execute( SQL )
    SQL = """insert into lipid_decision SET ID_Transaction='""" + str(transaction_id) + """'"""
    cursor.execute( SQL )
    
    SQL = """SELECT Date FROM transactions WHERE ID_Transaction='""" + str(transaction_id) + """';"""
    cursor.execute(SQL)
    date = cursor.fetchone()
    try:
        date = date[0]
    except:
        return None
    prevDisease = "True" if primary.lower() == "true" else "False"
    SQL = """insert into decide.patient_data SET ID_patient = '""" + str(patientID) + """', BP_Sys = '""" + str(systolic) + """', BP_Dia = '""" + str(diastolic) + """', HBA1c = '""" + str(hba1c) + """', Cholesterol = '""" + str(cholest) + """', Prev_disease = '""" + str(prevDisease) + """', Qrisk = '""" + str(qrisk) + """', Initial_Followup = 'Initial', Date = '""" + str(now) + """', Gly_Meds = '""" + str(gly_meds) + """', Lipid_Meds = '""" + str(lipid_meds) + """', Htn_Meds = '""" + str(htn_meds) + """';"""
    cursor.execute(SQL)
    conn.commit()
    conn.close()
    cursor.close()

    gly_res = decide_glycemic(glycemic_dict, glycelmic_meds_list, glycemicDict, int(hba1c))

    if "Patient is not included in the trial" in gly_res:
        return Response("<notIncluded>Patient is not included in the trial</notIncluded>", mimetype='text/xml')
            
    result = decide_htn(htn_dict, htn_meds_list, htnDict, systolic, diastolic, primary)
    result = result.replace("{{&bp}}", final_bp)
    result = result.replace("{{&prev}}", prevention)
    htn_res = result
    
    result = decide_lipid(lipid_dict, lipid_meds_list, lipidDict, qrisk, primary)
    result = result.replace("{{&qr}}", str(qrisk))
    result = result.replace("{{&prev}}", prevention)
    lipid_res = result
   
    ret_xml = xml.dom.minidom.parseString("<decideResult>" + gly_res + htn_res + lipid_res + xmlify("transactionID", str(transaction_id)) + "</decideResult>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
    
@webapp.route('/recordDecision', methods=['GET', 'POST'])
@check_logged_in
def record():
    if request.method == 'POST':
        transactionID = request.form['transactionID']
        diabeticDecision = request.form.getlist['diabeticDecision']
        diabeticDecision = concatenate_list(diabeticDecision)
        htnDecision = request.form.getlist['htnDecision']
        htnDecision = concatenate_list(htnDecision)
        lipidDecision = request.form.getlist['lipidDecision']
        lipidDecision = concatenate_list(lipidDecision)
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )

        cursor = conn.cursor()
        SQL = """update gly_decision SET Gly_Decision='""" + diabeticDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        SQL = """update htn_decision SET Htn_Decision='""" + htnDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        SQL = """update lipid_decision SET Lipid_Decision='""" + lipidDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        now = time.strftime('%Y-%m-%d %H:%M:%S')
        SQL = """update decide.transactions, decide.patient_data SET patient_data.Date = '""" + str(now) + """', transactions.Date = '""" + str(now) + """' WHERE patient_data.Date = transactions.Date AND ID_Transaction = '""" + str(transactionID) + """';"""
        cursor.execute( SQL )
        conn.commit()
        conn.close()
        cursor.close()
       
        return "Success!"    

    if request.method == 'GET':
        transactionID = request.args.get('transactionID')
        diabeticDecision = request.args.getlist('diabeticDecision')
        diabeticDecision = concatenate_list(diabeticDecision)
        htnDecision = request.args.getlist('htnDecision')
        htnDecision = concatenate_list(htnDecision)
        lipidDecision = request.args.getlist('lipidDecision')
        lipidDecision = concatenate_list(lipidDecision)
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )

        cursor = conn.cursor()
        SQL = """update gly_decision SET Gly_Decision='""" + diabeticDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        SQL = """update htn_decision SET Htn_Decision='""" + htnDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        SQL = """update lipid_decision SET Lipid_Decision='""" + lipidDecision + """' where ID_Transaction = """ + str(transactionID)
        cursor.execute( SQL )
        now = time.strftime('%Y-%m-%d %H:%M:%S')
        SQL = """update decide.transactions, decide.patient_data SET patient_data.Date = '""" + str(now) + """', transactions.Date = '""" + str(now) + """' WHERE patient_data.Date = transactions.Date AND ID_Transaction = '""" + str(transactionID) + """';"""
        cursor.execute( SQL )
        conn.commit()
        conn.close()
        cursor.close()
       
        return "Success!"      
        
@webapp.route('/listPatientTransactions')
@check_logged_in
def listPatientTransactions():
    patient_id = request.args.get('patientID')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT transactions.ID_Transaction, Date
            FROM transactions
            WHERE transactions.ID_patient = """ + str(patient_id)
    cursor.execute( SQL )
    ids = ""
    for row in cursor.fetchall():
        ids += xmlify("transaction", xmlify("id", str(row[0])) + xmlify("date", str(row[1])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<transactions>" + ids + "</transactions>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
 
@webapp.route('/listGpTransactions')
@check_logged_in
def listGpTransactions():
    gp_id = request.args.get('gpID')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT transactions.ID_Transaction, Date
            FROM transactions
            WHERE transactions.ID_GP = """ + str(gp_id)
    cursor.execute( SQL )
    ids = ""
    for row in cursor.fetchall():
        ids += xmlify("transaction", xmlify("id", str(row[0])) + xmlify("date", str(row[1])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<transactions>" + ids + "</transactions>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
 
@webapp.route('/getTransaction')
@check_logged_in
def getTransaction():
    transaction_id = request.args.get('id')
    
    info = ""
    id = ""
    date = ""
    dD = ""
    hD = ""
    lD = ""
    dDText = ""
    hDText = ""
    lDText = ""
    dOff = False
    hOff = False
    lOff = False
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT transactions.ID_Transaction, Date, gly_decision, htn_decision, lipid_decision
            FROM transactions, gly_decision, htn_decision, lipid_decision
            WHERE transactions.ID_Transaction = """ + transaction_id + """
            and transactions.ID_Transaction = gly_decision.ID_Transaction
            and transactions.ID_Transaction = htn_decision.ID_Transaction
            and transactions.ID_Transaction = lipid_decision.ID_Transaction"""    
    cursor.execute( SQL )

    for row in cursor.fetchall():
        id = str(row[0])
        date = str(row[1])
        dD = str(row[2])
        hD = str(row[3])
        lD = str(row[4])
    
    dRes = ""
    hRes = ""
    lRes = ""
    
    dDSplit = dD.split("&")   
    for d in dDSplit:
        try:
            SQL = """SELECT decision_ids.text FROM decision_ids WHERE decision_ids.id = """ + d + """;"""
            cursor.execute( SQL )
            res = cursor.fetchone()
            dRes += xmlify("diabeticDecision", xmlify("id", d) + xmlify("text", res[0]))
        except:
            dRes += xmlify("diabeticDecision", xmlify("id", "N/A") + xmlify("text", d))            
    
    hDSplit = hD.split("&") 
    for d in hDSplit:
        try:
            SQL = """SELECT decision_ids.text FROM decision_ids WHERE decision_ids.id = """ + d + """;"""
            cursor.execute( SQL )
            res = cursor.fetchone()
            dRes += xmlify("htnDecision", xmlify("id", d) + xmlify("text", res[0]))
        except:
            dRes += xmlify("htnDecision", xmlify("id", "N/A") + xmlify("text", d))  
            
                    
    lDSplit = lD.split("&")   
    for d in lDSplit:
        try:
            SQL = """SELECT decision_ids.text FROM decision_ids WHERE decision_ids.id = """ + d + """;"""
            cursor.execute( SQL )
            res = cursor.fetchone()
            dRes += xmlify("lipidDecision", xmlify("id", d) + xmlify("text", res[0]))
        except:
            dRes += xmlify("lipidDecision", xmlify("id", "N/A") + xmlify("text", d))                        
    
    info += xmlify("id", id) + xmlify("date", date) + dRes + hRes + lRes
    conn.close()
    cursor.close()

    return Response(xmlify("transaction", info), mimetype='text/xml')

@webapp.route('/getAllTransactions')
@check_logged_in
def getAllTransactions():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT transactions.ID_Transaction, Date, gly_decision, htn_decision, lipid_decision
            FROM transactions, gly_decision, htn_decision, lipid_decision
            WHERE transactions.ID_Transaction = gly_decision.ID_Transaction
            and transactions.ID_Transaction = htn_decision.ID_Transaction
            and transactions.ID_Transaction = lipid_decision.ID_Transaction"""    
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("transaction", xmlify("id", str(row[0])) + xmlify("date", str(row[1])) + xmlify("diabeticDecision", str(row[2])) + xmlify("htnDecision", str(row[3])) + xmlify("lipidDecision", str(row[4])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<transactions>" + info + "</transactions>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
    
@webapp.route('/registerUser', methods=['GET', 'POST'])
def registerUser():
    if request.method == 'POST':
        LastName = request.form['lastName']
        FirstName = request.form['firstName']
        Username = request.form['username']
        Password = pbkdf2_sha512.encrypt(request.form['password'], rounds=30000 , salt_size=16)
        Practice = request.form['practice']
        IC = request.form['ic']
        Email = request.form['email']
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        try:
            SQL = """insert into gp set GP_Fname = '""" + FirstName + """', GP_Lname = '""" + LastName + """', Practice = '""" + Practice + """', Username = '""" + Username + """', Password = '""" + Password + """', Intervention_Control = '""" + IC + """', Active = 'False', ActivationToken = '""" + token + """', Email = '""" + Email + """'"""
            cursor.execute( SQL )
        except:
            return Response("<error>Email address or username already in use.</error>", mimetype='text/xml')  

        SQL = """SELECT MAX( ID_GP ) FROM gp"""
        cursor.execute(SQL)
        pat_id = cursor.fetchone()
        try:
            pat_id = pat_id[0]
        except:
            return None
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = 'http://' + m_ip + '/activate?e=' + Email + '&t=' + token
        send_mail(Email, 'RCSI - Decide Accout Verification', 'Please use the following link to activate your account: ' + activate_link)
        return Response("<status><gpID>" + str(pat_id) + "</gpID><info>Confirmation email sent.</info></status>", mimetype='text/xml')  


    if request.method == 'GET':
        LastName = request.args.get('lastName')
        FirstName = request.args.get('firstName')
        Username = request.args.get('username')
        Password = pbkdf2_sha512.encrypt(request.args.get('password'), rounds=30000 , salt_size=16)
        Practice = request.args.get('practice')
        IC = request.args.get('ic')
        Email = request.args.get('email')
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        try:
            SQL = """insert into gp set GP_Fname = '""" + FirstName + """', GP_Lname = '""" + LastName + """', Practice = '""" + Practice + """', Username = '""" + Username + """', Password = '""" + Password + """', Intervention_Control = '""" + IC + """', Active = 'False', ActivationToken = '""" + token + """', Email = '""" + Email + """'"""
            cursor.execute( SQL )
        except:
            return Response("<error>Email address or username already in use.</error>", mimetype='text/xml')  

        SQL = """SELECT MAX( ID_GP ) FROM gp"""
        cursor.execute(SQL)
        pat_id = cursor.fetchone()
        try:
            pat_id = pat_id[0]
        except:
            return None
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = 'http://' + m_ip + '/activate?e=' + Email + '&t=' + token
        send_mail(Email, 'RCSI - Decide Accout Verification', 'Please use the following link to activate your account: ' + activate_link)
        return Response("<status><gpID>" + str(pat_id) + "</gpID><info>Confirmation email sent.</info></status>", mimetype='text/xml') 
        
@webapp.route('/activate') #methods=['POST']
def activate():
    Email = request.args.get('e')
    Token = request.args.get('t')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """SELECT * FROM gp WHERE Email='""" + Email + """' and ActivationToken='""" + Token + """'"""
    cursor.execute( SQL )
    res = cursor.fetchone()
    if res is None:
        conn.commit()
        conn.close()
        cursor.close()        
        return("Invalid activation link.")
    
    
    SQL = """UPDATE gp SET Active='True', ActivationToken='x' WHERE Email='""" + Email + """' and ActivationToken='""" + Token + """'"""
    cursor.execute( SQL )
    conn.commit()
    conn.close()
    cursor.close()
    
    return ("Account activated. You can now log in.")
    
@webapp.route('/registerPatient', methods=['GET', 'POST']) #methods=['POST']
def registerPatient():
    if request.method == 'POST':
        Age = request.form['age']
        Gender = request.form['gender']
        Duration = request.form['duration']
        GMS = request.form['gms']
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """insert into patient set Age = '""" + Age + """', Gender = '""" + Gender + """', Duration = '""" + Duration + """', GMS = '""" + GMS + """'"""
        cursor.execute( SQL )
        SQL = """SELECT MAX( ID_patient ) FROM patient"""
        cursor.execute(SQL)
        pat_id = cursor.fetchone()
        try:
            pat_id = pat_id[0]
        except:
            return None
        conn.commit()
        conn.close()
        cursor.close()
        return Response("<patientID>" + str(pat_id) + "</patientID>", mimetype='text/xml')  

    elif request.method == 'GET':
        Age = request.args.get('age')
        Gender = request.args.get('gender')
        Duration = request.args.get('duration')
        GMS = request.args.get('gms')
        gp = request.args.get('gpID')
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """insert into patient set Age = '""" + Age + """', Gender = '""" + Gender + """', Duration = '""" + Duration + """', GMS = '""" + GMS + """'"""
        cursor.execute( SQL )
        SQL = """SELECT MAX( ID_patient ) FROM patient"""
        cursor.execute(SQL)
        pat_id = cursor.fetchone()
        try:
            pat_id = pat_id[0]
        except:
            return None
        
        SQL = """insert into gp_patient set ID_GP = '""" + str(gp) + """', ID_patient = '""" + str(pat_id) + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        return Response("<patientID>" + str(pat_id) + "</patientID>", mimetype='text/xml')          
    
@webapp.route('/login', methods=['GET', 'POST']) #methods=['POST']
def login():
    if request.method == 'POST':

        Username = request.form['username']
        Password = request.form['password']

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        
        cursor = conn.cursor()
        SQL = """select Password, Active, ID_GP from gp where Username = '""" + Username + """';"""
        cursor.execute( SQL )
        res = cursor.fetchone()

        if res is None:
            conn.commit()
            conn.close()
            cursor.close()        
            return(xmlify("loginStatus", "Username doesn't exist"))

        try:
            enc_password = res[0]
            active = res[1]
            gp_id = res[2]
        except:
            return None
        
        conn.commit()
        conn.close()
        cursor.close()
        
        valid = pbkdf2_sha512.verify(Password, enc_password)
        if valid:
            if active == 'False':
                return(xmlify("loginStatus", "Please activate your account"))
            else:
                session['logged-in'] = True
                session['userid'] = Username
                session['passwd'] = Password
                res = xmlify("login", "You are now logged in") + xmlify("id", str(gp_id))
                return Response(xmlify("loginStatus", res), mimetype='text/xml') 
        else:
            return Response(xmlify("loginStatus", "Invalid password"), mimetype='text/xml')      
    elif request.method == 'GET':
        Username = request.args.get('username')
        Password = request.args.get('password')
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """select Password, Active, ID_GP from gp where Username = '""" + Username + """';"""
        cursor.execute( SQL )
        res = cursor.fetchone()
        if res is None:
            conn.commit()
            conn.close()
            cursor.close()   
            
            return(xmlify("loginStatus", "Username doesn't exist"))
            
        try:
            enc_password = res[0]
            active = res[1]
            gp_id = res[2]
        except:
            return None
        conn.commit()
        conn.close()
        cursor.close()
        valid = pbkdf2_sha512.verify(Password, enc_password)
        if valid:
            if active == 'False':
                return(xmlify("loginStatus", "Please activate your account"))
            else:
                session['logged-in'] = True
                session['userid'] = Username
                session['passwd'] = Password
                res = xmlify("login", "You are now logged in") + xmlify("id", str(gp_id))
                return Response(xmlify("loginStatus", res), mimetype='text/xml') 
        else:
            return Response(xmlify("loginStatus", "Invalid password"), mimetype='text/xml')      
            
@webapp.route('/logout')
@check_logged_in
def dologout():
    session.pop('logged-in', None)
    session.pop('userid', None)
    session.pop('passwd', None)   
    return Response(xmlify("loginStatus", "You are now logged out"), mimetype='text/xml') 
   
@webapp.route('/resendMail', methods=['GET', 'POST']) #methods=['POST']
def resendMail():
    if request.method == 'POST':
        Email = request.form['e']
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """Select ActivationToken, Active from gp Where Email='""" + Email + """'"""
        cursor.execute( SQL )
        res = cursor.fetchone()
        
        if res is None:
            conn.commit()
            conn.close()
            cursor.close()
            return("Email address not found. Please sign up.")
        
        if res[1] == 'True':
            conn.commit()
            conn.close()
            cursor.close()
            return("Account is already activated.")
        res = res[0]
        
        SQL = """UPDATE gp SET ActivationToken='""" + token + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = 'http://' + m_ip + '/activate?e=' + Email + '&t=' + token
        send_mail(Email, 'RCSI - Decide Verification Link', 'Use the following link to activate your account: ' + activate_link)

        return ("Mail sent.")
        
    if request.method == 'GET':
        Email = request.args.get('e')
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """Select ActivationToken, Active from gp Where Email='""" + Email + """'"""
        cursor.execute( SQL )
        res = cursor.fetchone()
        
        if res is None:
            conn.commit()
            conn.close()
            cursor.close()
            return("Email address not found. Please sign up.")
        
        if res[1] == 'True':
            conn.commit()
            conn.close()
            cursor.close()
            return("Account is already activated.")
        res = res[0]
        
        SQL = """UPDATE gp SET ActivationToken='""" + token + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = 'http://' + m_ip + '/activate?e=' + Email + '&t=' + token
        send_mail(Email, 'RCSI - Decide Verification Link', 'Use the following link to activate your account: ' + activate_link)

        return ("Mail sent.")        

@webapp.route('/checkUsername') #methods=['POST']
def checkUsername():
    Name = request.args.get('n')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """Select Username from gp Where Username='""" + Name + """'"""
    cursor.execute( SQL )
    res = cursor.fetchone()
    
    conn.commit()
    conn.close()
    cursor.close()
    
    if res is None:
        return("Ok.")
    else:
        return("Username exists.")
    

@webapp.route('/checkEmail') #methods=['POST']
def checkEmail():
    Email = request.args.get('e')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """Select Email from gp Where Email='""" + Email + """'"""
    cursor.execute( SQL )
    res = cursor.fetchone()
    
    conn.commit()
    conn.close()
    cursor.close()
    
    if res is None:
        return("Ok.")
    else:
        return("Email exists.")
    
@webapp.route('/getIDs') #methods=['POST']
def getIDs():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """SELECT * FROM decide.decision_ids ORDER BY id""" 
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("decision", xmlify("id", str(row[0])) + xmlify("text", str(row[1])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<decisions>" + info + "</decisions>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')   

@webapp.route('/getID') #methods=['POST']
def getID():
    id = request.args.get('id')
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """SELECT decision_ids.text FROM decide.decision_ids WHERE id = """ + id + """;""" 
    cursor.execute( SQL )
    info = ""
    res = cursor.fetchone()[0]
    conn.close()
    cursor.close()
    res = xmlify("text", str(res))
    return Response(res, mimetype='text/xml')    
    
@webapp.route('/getPatientsByGP')
@check_logged_in
def getPatientsByGP():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    gid = request.args.get('gpID')
    cursor = conn.cursor()
    SQL = """SELECT gp_patient.ID_patient, Age, Gender, Duration, GMS 
            FROM decide.gp_patient, decide.patient 
            WHERE ID_GP = '""" + str(gid) + """' and gp_patient.ID_patient = patient.ID_patient;""" 
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("patient", xmlify("id", str(row[0])) + xmlify("age", str(row[1])) + xmlify("gender", str(row[2])) + xmlify("duration", str(row[3])) + xmlify("GMS", str(row[4])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<patients>" + info + "</patients>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')    

@webapp.route('/getAllPatients')
@check_logged_in
def getAllPatients():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """SELECT * FROM decide.patient""" 
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("patient", xmlify("id", str(row[0])) + xmlify("age", str(row[1])) + xmlify("gender", str(row[2])) + xmlify("duration", str(row[3])) + xmlify("GMS", str(row[4])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<patients>" + info + "</patients>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')        

@webapp.route('/getPatient')
@check_logged_in
def getPatient():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    
    pid = request.args.get('pID')
    SQL = """SELECT * FROM decide.patient WHERE ID_patient = '""" + str(pid) + """'""" 
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("patient", xmlify("id", str(row[0])) + xmlify("age", str(row[1])) + xmlify("gender", str(row[2])) + xmlify("duration", str(row[3])) + xmlify("GMS", str(row[4])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString(info)
    return Response(ret_xml.toprettyxml(), mimetype='text/xml') 
 
@webapp.route('/requestPasswordReset', methods=['GET', 'POST']) #methods=['POST']
def requestPasswordReset():
    if request.method == 'POST':
        Email = request.form['e']
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """Select ActivationToken, Active from gp Where Email='""" + Email + """'"""
        cursor.execute( SQL )
        res = cursor.fetchone()
        
        if res is None:
            conn.commit()
            conn.close()
            cursor.close()
            return("Email address not found. Please sign up.")
        
        if res[1] == 'False':
            conn.commit()
            conn.close()
            cursor.close()
            return("Account is not activated. Please navigate to the resend email page.")
        res = res[0]
        
        SQL = """UPDATE gp SET ActivationToken='""" + token + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = token #Proper link here?
        send_mail(Email, 'RCSI - Decide. Password Reset Requested.', 'A password reset was requested for this account. Please copy and paste the following token into the reset field: ' + activate_link)

        return ("Reset mail sent.")

    if request.method == 'GET':
        Email = request.args.get('e')
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")
        
        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        SQL = """Select ActivationToken, Active from gp Where Email='""" + Email + """'"""
        cursor.execute( SQL )
        res = cursor.fetchone()
        
        if res is None:
            conn.commit()
            conn.close()
            cursor.close()
            return("Email address not found. Please sign up.")
        
        if res[1] == 'False':
            conn.commit()
            conn.close()
            cursor.close()
            return("Account is not activated. Please navigate to the resend email page.")
        res = res[0]
        
        SQL = """UPDATE gp SET ActivationToken='""" + token + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = token #Proper link here?
        send_mail(Email, 'RCSI - Decide. Password Reset Requested.', 'A password reset was requested for this account. Please copy and paste the following token into the reset field: ' + activate_link)

        return ("Reset mail sent.")
        
@webapp.route('/resetPassword', methods=['GET', 'POST']) #methods=['POST']
def resetPassword():
    if request.method == 'POST':
        Email = request.form['e']
        Token = request.form['t']
        Password = pbkdf2_sha512.encrypt(request.form['p'], rounds=30000 , salt_size=16)

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        
        SQL = """UPDATE gp SET ActivationToken='x', Password='""" + str(Password) + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        return ("Password reset. You can now login.")
    if request.method == 'GET':
        Email = request.args.get('e')
        Token = request.args.get('t')
        Password = pbkdf2_sha512.encrypt(request.args.get('p'), rounds=30000 , salt_size=16)

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        
        SQL = """UPDATE gp SET ActivationToken='x', Password='""" + str(Password) + """' WHERE Email='""" + Email + """'"""
        cursor.execute( SQL )
        
        conn.commit()
        conn.close()
        cursor.close()
        
        return ("Password reset. You can now login.") 

@check_logged_in
@webapp.route('/listTransactions')
def listTransactions():
    patient_id = request.args.get('patientID')
    
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT transactions.ID_Transaction, Date
            FROM transactions
            WHERE transactions.ID_patient = """ + str(patient_id)
    cursor.execute( SQL )
    ids = ""
    for row in cursor.fetchall():
        ids += xmlify("transaction", xmlify("id", str(row[0])) + xmlify("date", str(row[1])))
    conn.commit()
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<transactions>" + ids + "</transactions>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
        
@check_logged_in
@webapp.route('/getData')
def getData():
    transaction_id = request.args.get('id')
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    cursor = conn.cursor()
    SQL = """SELECT * FROM decide.patient_data, decide.transactions WHERE patient_data.Date = transactions.Date AND transactions.ID_Transaction = '""" + str(transaction_id) + """';"""
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("PatientID", str(row[1])) + xmlify("Systolic", str(row[2])) + xmlify("Diastolic", str(row[3])) + xmlify("HBA1c", str(row[4])) + xmlify("Cholesterol", str(row[5]))  + xmlify("PrevDisease", str(row[6])) + xmlify("QRisk", str(row[7])) + xmlify("InitialOrFollowup", str(row[8])) + xmlify("Date", str(row[9]))
        gly_meds_parsed = str(row[10]).split("&")
        gly_meds = ""
        for i in gly_meds_parsed:
            gly_meds += xmlify("med", i)
        gly_meds = xmlify("glyMeds", gly_meds)
        htn_meds_parsed = str(row[11]).split("&")
        htn_meds = ""
        for i in htn_meds_parsed:
            htn_meds += xmlify("med", i)
        htn_meds = xmlify("htnMeds", htn_meds)
        lipid_meds_parsed = str(row[12]).split("&")
        lipid_meds = ""
        for i in lipid_meds_parsed:
            lipid_meds += xmlify("med", i)
        lipid_meds = xmlify("lipidMeds", lipid_meds)
        
        info += gly_meds + htn_meds + lipid_meds
    
    info = xmlify("PatientData", info)
    
    conn.commit()
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString(info)
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
        
@webapp.route('/getLoggedIn')
@check_logged_in
def getLoggedIn():
    return Response(xmlify("loginStatus", "You are logged in"), mimetype='text/xml')                    
        
@webapp.route('/getGPByID')
@check_logged_in
def getGPByID():
    id = request.args.get('id')

    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT * from gp WHERE ID_GP = """ + str(id)   
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("id", xmlify("id", str(row[0])) + xmlify("firstName", str(row[1])) + xmlify("lastName", str(row[2])) + xmlify("practice", str(row[3])) + xmlify("username", str(row[4])) + xmlify("interventionControl", str(row[6])) + xmlify("email", str(row[9])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<gp>" + info + "</gp>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')        

@webapp.route('/getGPByUsername')
@check_logged_in
def getGPByUsername():
    id = request.args.get('name')

    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT * from gp WHERE Username = '""" + str(id) + """'"""
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("id", xmlify("id", str(row[0])) + xmlify("firstName", str(row[1])) + xmlify("lastName", str(row[2])) + xmlify("practice", str(row[3])) + xmlify("username", str(row[4])) + xmlify("interventionControl", str(row[6])) + xmlify("email", str(row[9])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<gp>" + info + "</gp>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')

@webapp.route('/getGPByEmail')
@check_logged_in
def getGPByEmail():
    id = request.args.get('email')

    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )

    cursor = conn.cursor()
    SQL = """SELECT * from gp WHERE Email = '""" + str(id) + """'"""
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("id", xmlify("id", str(row[0])) + xmlify("firstName", str(row[1])) + xmlify("lastName", str(row[2])) + xmlify("practice", str(row[3])) + xmlify("username", str(row[4])) + xmlify("interventionControl", str(row[6])) + xmlify("email", str(row[9])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<gp>" + info + "</gp>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')
    
@webapp.errorhandler(500)
def internal_error(error):
    traceback.print_exc()    
    return None
