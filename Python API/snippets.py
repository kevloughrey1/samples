#regex functionality to replace templated text between the characters {{}} with correct values
#i.e. passing the function ("quick&dog", "the {{1}} brown fox jumps over the lazy {{2}}") will result in
#the output "the quick brown fox jumps over the lazy dog"
def regex_replace(nodes, text):
    regex = re.compile('{{.*?}}')
    matches = regex.findall(nodes[0])
    
    temps_list = text.split("&")
    for val in matches:
        if "&" not in val:
            val1 = val.replace('{', '')
            val1 = val1.replace('}', '')
            newval = int(val1) - 1
            nodes = [w.replace(val, temps_list[newval]) for w in nodes]
    return nodes

#send an email
def send_mail(to, subject, mesg):
    m_from = '***@gmail.com'
    m_to = to
    msg = MIMEMultipart()
    msg['From'] = m_from
    msg['To'] = m_to
    msg['Subject'] = subject
    message = mesg
    msg.attach(MIMEText(message))
    mailserver = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    mailserver.login(m_from, '***')
    mailserver.ehlo()
    mailserver.sendmail(m_from,m_to,msg.as_string())
    mailserver.close()


#function decorator
def check_logged_in(func):
    """ This decorator function checks to see that a valid user is 
        logged in. If yes, run the called function. If not, send
        the user to the login page. 
    """
    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if 'logged-in' in session:
            return(func(*args, **kwargs))
        else:
            return(func(*args, **kwargs))
    return wrapped_function    


#user account registration
@webapp.route('/registerUser', methods=['GET', 'POST'])
def registerUser():
    if request.method == 'POST':
        LastName = request.form['lastName']
        FirstName = request.form['firstName']
        Username = request.form['username']
        Password = pbkdf2_sha512.encrypt(request.form['password'], rounds=30000 , salt_size=16)
        Practice = request.form['practice']
        IC = request.form['ic']
        Email = request.form['email']
        
        rand = os.urandom(32)
        token = binascii.hexlify(rand).decode("utf-8")

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        cursor = conn.cursor()
        try:
            SQL = """insert into gp set GP_Fname = '""" + FirstName + """', GP_Lname = '""" + LastName + """', Practice = '""" + Practice + """', Username = '""" + Username + """', Password = '""" + Password + """', Intervention_Control = '""" + IC + """', Active = 'False', ActivationToken = '""" + token + """', Email = '""" + Email + """'"""
            cursor.execute( SQL )
        except:
            return Response("<error>Email address or username already in use.</error>", mimetype='text/xml')  

        SQL = """SELECT MAX( ID_GP ) FROM gp"""
        cursor.execute(SQL)
        pat_id = cursor.fetchone()
        try:
            pat_id = pat_id[0]
        except:
            return None
        conn.commit()
        conn.close()
        cursor.close()
        
        activate_link = 'http://' + m_ip + '/activate?e=' + Email + '&t=' + token
        send_mail(Email, 'RCSI - Decide Accout Verification', 'Please use the following link to activate your account: ' + activate_link)
        return Response("<status><gpID>" + str(pat_id) + "</gpID><info>Confirmation email sent.</info></status>", mimetype='text/xml')  


#login function
@webapp.route('/login', methods=['GET', 'POST']) #methods=['POST']
def login():
    if request.method == 'POST':

        Username = request.form['username']
        Password = request.form['password']

        import mysql.connector
        conn = mysql.connector.connect(host=d_host,
                               user=d_user, 
                               password=d_password, 
                               database=d_database, )
        
        cursor = conn.cursor()
        SQL = """select Password, Active, ID_GP from gp where Username = '""" + Username + """';"""
        cursor.execute( SQL )
        res = cursor.fetchone()

        if res is None:
            conn.commit()
            conn.close()
            cursor.close()        
            return(xmlify("loginStatus", "Username doesn't exist"))

        try:
            enc_password = res[0]
            active = res[1]
            gp_id = res[2]
        except:
            return None
        
        conn.commit()
        conn.close()
        cursor.close()
        
        valid = pbkdf2_sha512.verify(Password, enc_password)
        if valid:
            if active == 'False':
                return(xmlify("loginStatus", "Please activate your account"))
            else:
                session['logged-in'] = True
                session['userid'] = Username
                session['passwd'] = Password
                res = xmlify("login", "You are now logged in") + xmlify("id", str(gp_id))
                return Response(xmlify("loginStatus", res), mimetype='text/xml') 
        else:
            return Response(xmlify("loginStatus", "Invalid password"), mimetype='text/xml')  


#get info for patients associated with a particular GP
@webapp.route('/getPatientsByGP')
@check_logged_in
def getPatientsByGP():
    import mysql.connector
    conn = mysql.connector.connect(host=d_host,
                           user=d_user, 
                           password=d_password, 
                           database=d_database, )
    gid = request.args.get('gpID')
    cursor = conn.cursor()
    SQL = """SELECT gp_patient.ID_patient, Age, Gender, Duration, GMS 
            FROM decide.gp_patient, decide.patient 
            WHERE ID_GP = '""" + str(gid) + """' and gp_patient.ID_patient = patient.ID_patient;""" 
    cursor.execute( SQL )
    info = ""
    for row in cursor.fetchall():
        info += xmlify("patient", xmlify("id", str(row[0])) + xmlify("age", str(row[1])) + xmlify("gender", str(row[2])) + xmlify("duration", str(row[3])) + xmlify("GMS", str(row[4])))
    conn.close()
    cursor.close()
    ret_xml = xml.dom.minidom.parseString("<patients>" + info + "</patients>")
    return Response(ret_xml.toprettyxml(), mimetype='text/xml')   