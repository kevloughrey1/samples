CREATE DATABASE  IF NOT EXISTS `decide` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `decide`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: decide
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gly_decision`
--

DROP TABLE IF EXISTS `gly_decision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gly_decision` (
  `ID_Gly_Decision` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Transaction` int(11) NOT NULL,
  `Gly_Decision` varchar(500) NOT NULL,
  PRIMARY KEY (`ID_Gly_Decision`),
  UNIQUE KEY `ID_Gly_Decision_UNIQUE` (`ID_Gly_Decision`),
  KEY `ID_Transaction_gly_decision_idx` (`ID_Transaction`),
  CONSTRAINT `ID_Transaction_gly_decision` FOREIGN KEY (`ID_Transaction`) REFERENCES `transactions` (`ID_Transaction`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gly_decision`
--

LOCK TABLES `gly_decision` WRITE;
/*!40000 ALTER TABLE `gly_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `gly_decision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gp`
--

DROP TABLE IF EXISTS `gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gp` (
  `ID_GP` int(11) NOT NULL,
  `GP_Fname` varchar(20) NOT NULL,
  `GP_Lname` varchar(45) NOT NULL,
  `Practice` varchar(50) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Intervention_Control` enum('Intervention','Control') NOT NULL,
  PRIMARY KEY (`ID_GP`),
  UNIQUE KEY `Username_UNIQUE` (`Username`),
  UNIQUE KEY `Password_UNIQUE` (`Password`),
  UNIQUE KEY `ID_GP_UNIQUE` (`ID_GP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gp`
--

LOCK TABLES `gp` WRITE;
/*!40000 ALTER TABLE `gp` DISABLE KEYS */;
INSERT INTO `gp` VALUES (1,'John','Smith','Fake Medical Clinic','JohnSmith123','3a5500db31c6y629a23c1ac6bdd37452','Intervention');
/*!40000 ALTER TABLE `gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gp_patient`
--

DROP TABLE IF EXISTS `gp_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gp_patient` (
  `ID_GP_Patient` int(11) NOT NULL AUTO_INCREMENT,
  `ID_GP` int(11) NOT NULL,
  `ID_patient` int(11) NOT NULL,
  PRIMARY KEY (`ID_GP_Patient`),
  UNIQUE KEY `ID_GP_Patient_UNIQUE` (`ID_GP_Patient`),
  UNIQUE KEY `ID_patient_UNIQUE` (`ID_patient`),
  KEY `ID_GP_idx` (`ID_GP`),
  KEY `ID_patient_idx` (`ID_patient`),
  CONSTRAINT `ID_GP` FOREIGN KEY (`ID_GP`) REFERENCES `gp` (`ID_GP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_patient_GP_Patient` FOREIGN KEY (`ID_patient`) REFERENCES `patient` (`ID_patient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gp_patient`
--

LOCK TABLES `gp_patient` WRITE;
/*!40000 ALTER TABLE `gp_patient` DISABLE KEYS */;
INSERT INTO `gp_patient` VALUES (1,1,2),(3,1,5),(4,1,6),(5,1,7),(6,1,8);
/*!40000 ALTER TABLE `gp_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `htn_decision`
--

DROP TABLE IF EXISTS `htn_decision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `htn_decision` (
  `ID_htn_Decision` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Transaction` int(11) NOT NULL,
  `Htn_Decision` varchar(500) NOT NULL,
  PRIMARY KEY (`ID_htn_Decision`),
  UNIQUE KEY `ID_htn_Decision_UNIQUE` (`ID_htn_Decision`),
  KEY `ID_Transaction_htn_decision_idx` (`ID_Transaction`),
  CONSTRAINT `ID_Transaction_htn_decision` FOREIGN KEY (`ID_Transaction`) REFERENCES `transactions` (`ID_Transaction`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `htn_decision`
--

LOCK TABLES `htn_decision` WRITE;
/*!40000 ALTER TABLE `htn_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `htn_decision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lipid_decision`
--

DROP TABLE IF EXISTS `lipid_decision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lipid_decision` (
  `ID_htn_Decision` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Transaction` int(11) NOT NULL,
  `Lipid_Decision` varchar(500) NOT NULL,
  PRIMARY KEY (`ID_htn_Decision`),
  UNIQUE KEY `ID_htn_Decision_UNIQUE` (`ID_htn_Decision`),
  KEY `ID_Transaction_lipid_decision_idx` (`ID_Transaction`),
  CONSTRAINT `ID_Transaction_lipid_decision` FOREIGN KEY (`ID_Transaction`) REFERENCES `transactions` (`ID_Transaction`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lipid_decision`
--

LOCK TABLES `lipid_decision` WRITE;
/*!40000 ALTER TABLE `lipid_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `lipid_decision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `ID_Log` int(11) NOT NULL AUTO_INCREMENT,
  `Page` varchar(45) NOT NULL,
  `ID_GP` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `IP` varchar(45) NOT NULL,
  `ID_patient` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_Log`),
  UNIQUE KEY `ID_Log_UNIQUE` (`ID_Log`),
  KEY `ID_GP_logs_idx` (`ID_GP`),
  KEY `ID_Patient_logs_idx` (`ID_patient`),
  CONSTRAINT `ID_GP_logs` FOREIGN KEY (`ID_GP`) REFERENCES `gp` (`ID_GP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_Patient_logs` FOREIGN KEY (`ID_patient`) REFERENCES `patient` (`ID_patient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=490 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,'d',1,'2015-09-13 00:00:00','1',NULL),(2,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:10:47','10.1.8.131',NULL),(3,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:11:19','10.1.8.131',NULL),(4,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:11:45','10.1.8.131',NULL),(5,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:36:42','10.1.8.131',NULL),(6,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:40:07','10.1.8.131',NULL),(7,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:40:29','10.1.8.131',NULL),(8,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:41:10','10.1.8.131',NULL),(9,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:44:21','10.1.8.131',NULL),(10,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 12:46:14','10.1.8.131',NULL),(11,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 13:47:54','10.1.8.131',NULL),(12,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:19:30','10.1.8.131',NULL),(13,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:22:56','10.1.8.131',NULL),(14,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:23:27','10.1.8.131',NULL),(15,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:24:52','10.1.8.131',NULL),(16,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:25:17','10.1.8.131',NULL),(17,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:25:38','10.1.8.131',NULL),(18,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:29:58','10.1.8.131',NULL),(19,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:30:21','10.1.8.131',NULL),(20,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:30:26','10.1.8.131',NULL),(21,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:32:05','10.1.8.131',NULL),(22,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 14:32:54','10.1.8.131',NULL),(23,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 16:21:08','10.1.8.131',NULL),(24,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 16:21:14','10.1.8.131',NULL),(25,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 16:21:27','10.1.8.131',NULL),(26,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 16:21:30','10.1.8.131',NULL),(27,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 16:27:45','10.1.8.131',NULL),(28,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 16:27:47','10.1.8.131',NULL),(29,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 16:27:59','10.1.8.131',NULL),(30,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 16:28:08','10.1.8.131',NULL),(31,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:00:14','10.1.8.131',NULL),(32,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:00:15','10.1.8.131',NULL),(33,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:00:15','10.1.8.131',NULL),(34,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:03','10.1.8.131',NULL),(35,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:01:07','10.1.8.131',NULL),(36,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:09','10.1.8.131',NULL),(37,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:10','10.1.8.131',NULL),(38,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:12','10.1.8.131',NULL),(39,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:55','10.1.8.131',NULL),(40,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:01:58','10.1.8.131',NULL),(41,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:01:59','10.1.8.131',NULL),(42,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:00','10.1.8.131',NULL),(43,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:02','10.1.8.131',NULL),(44,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:03','10.1.8.131',NULL),(45,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:04','10.1.8.131',NULL),(46,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:04','10.1.8.131',NULL),(47,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:04','10.1.8.131',NULL),(48,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:14','10.1.8.131',NULL),(49,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:15','10.1.8.131',NULL),(50,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:53','10.1.8.131',NULL),(51,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:56','10.1.8.131',NULL),(52,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:57','10.1.8.131',NULL),(53,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:02:58','10.1.8.131',NULL),(54,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:02:59','10.1.8.131',NULL),(55,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:03:04','10.1.8.131',NULL),(56,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:03:07','10.1.8.131',NULL),(57,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:03:30','10.1.8.131',NULL),(58,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:03:33','10.1.8.131',NULL),(59,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:05:21','10.1.8.131',NULL),(60,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:05:22','10.1.8.131',NULL),(61,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:10:52','10.1.8.131',NULL),(62,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:10:54','10.1.8.131',NULL),(63,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:11:14','10.1.8.131',NULL),(64,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:11:49','10.1.8.131',NULL),(65,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:11:56','10.1.8.131',NULL),(66,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:12:09','10.1.8.131',NULL),(67,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:13:43','10.1.8.131',NULL),(68,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:13:44','10.1.8.131',NULL),(69,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:13:46','10.1.8.131',NULL),(70,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:15:03','10.1.8.131',NULL),(71,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:15:15','10.1.8.131',NULL),(72,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:16:06','10.1.8.131',NULL),(73,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:20','10.1.8.131',NULL),(74,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:18:24','10.1.8.131',NULL),(75,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:25','10.1.8.131',NULL),(76,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:30','10.1.8.131',NULL),(77,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:18:33','10.1.8.131',NULL),(78,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:34','10.1.8.131',NULL),(79,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:46','10.1.8.131',NULL),(80,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:18:48','10.1.8.131',NULL),(81,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:50','10.1.8.131',NULL),(82,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:18:58','10.1.8.131',NULL),(83,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:19:30','10.1.8.131',NULL),(84,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:19:44','10.1.8.131',NULL),(85,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:20:48','10.1.8.131',NULL),(86,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:22:27','10.1.8.131',NULL),(87,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:22:29','10.1.8.131',NULL),(88,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:22:31','10.1.8.131',NULL),(89,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:22:33','10.1.8.131',NULL),(90,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:22:37','10.1.8.131',NULL),(91,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:23:44','10.1.8.131',NULL),(92,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:25:05','10.1.8.131',NULL),(93,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:26:02','10.1.8.131',NULL),(94,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:30:01','10.1.8.131',NULL),(95,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:30:06','10.1.8.131',NULL),(96,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:30:22','10.1.8.131',NULL),(97,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:30:29','10.1.8.131',NULL),(98,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:30:38','10.1.8.131',NULL),(99,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:33:16','10.1.8.131',NULL),(100,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:33:34','10.1.8.131',NULL),(101,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:33:45','10.1.8.131',NULL),(102,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:37:18','10.1.8.131',NULL),(103,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:37:20','10.1.8.131',NULL),(104,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:37:48','10.1.8.131',NULL),(105,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:38:05','10.1.8.131',NULL),(106,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:38:41','10.1.8.131',NULL),(107,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:39:23','10.1.8.131',NULL),(108,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:39:51','10.1.8.131',NULL),(109,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:40:10','10.1.8.131',NULL),(110,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:40:24','10.1.8.131',NULL),(111,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:40:47','10.1.8.131',NULL),(112,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:42:09','10.1.8.131',NULL),(113,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:42:12','10.1.8.131',NULL),(114,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:42:13','10.1.8.131',NULL),(115,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-26 17:42:16','10.1.8.131',NULL),(116,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-26 17:42:17','10.1.8.131',NULL),(117,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:29:51','10.1.8.131',NULL),(118,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:29:52','10.1.8.131',NULL),(119,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:29:55','10.1.8.131',NULL),(120,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:30:24','10.1.8.131',NULL),(121,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:30:26','10.1.8.131',NULL),(122,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:33:09','10.1.8.131',NULL),(123,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:33:10','10.1.8.131',NULL),(124,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:33:58','10.1.8.131',NULL),(125,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:34:02','10.1.8.131',NULL),(126,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:34:06','10.1.8.131',NULL),(127,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:38:16','10.1.8.131',NULL),(128,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:38:26','10.1.8.131',NULL),(129,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:13','10.1.8.131',NULL),(130,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:29','10.1.8.131',NULL),(131,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:30','10.1.8.131',NULL),(132,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:30','10.1.8.131',NULL),(133,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:39:33','10.1.8.131',NULL),(134,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:36','10.1.8.131',NULL),(135,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:46','10.1.8.131',NULL),(136,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:39:49','10.1.8.131',NULL),(137,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:39:51','10.1.8.131',NULL),(138,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:41:24','10.1.8.131',NULL),(139,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:41:39','10.1.8.131',NULL),(140,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:42:28','10.1.8.131',NULL),(141,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:42:50','10.1.8.131',NULL),(142,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:45:52','10.1.8.131',NULL),(143,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:50:55','10.1.8.131',NULL),(144,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:51:01','10.1.8.131',NULL),(145,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 11:51:02','10.1.8.131',NULL),(146,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:51:05','10.1.8.131',NULL),(147,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:54:50','10.1.8.131',NULL),(148,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:55:10','10.1.8.131',NULL),(149,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:05','10.1.8.131',NULL),(150,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:21','10.1.8.131',NULL),(151,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:29','10.1.8.131',NULL),(152,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:35','10.1.8.131',NULL),(153,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:40','10.1.8.131',NULL),(154,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:41','10.1.8.131',NULL),(155,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:46','10.1.8.131',NULL),(156,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:47','10.1.8.131',NULL),(157,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:50','10.1.8.131',NULL),(158,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:51','10.1.8.131',NULL),(159,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:51','10.1.8.131',NULL),(160,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 11:56:54','10.1.8.131',NULL),(161,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:02:19','10.1.8.131',NULL),(162,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:02:22','10.1.8.131',NULL),(163,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:02:25','10.1.8.131',NULL),(164,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:02:28','10.1.8.131',NULL),(165,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:02:37','10.1.8.131',NULL),(166,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:02:39','10.1.8.131',NULL),(167,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:07:37','10.1.8.131',NULL),(168,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:07:38','10.1.8.131',NULL),(169,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:07:41','10.1.8.131',NULL),(170,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:07:43','10.1.8.131',NULL),(171,'gp-dt-13.rcsi-internal.ie/DECIDE/patient.php',1,'2016-01-27 12:07:44','10.1.8.131',NULL),(172,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:07:45','10.1.8.131',NULL),(173,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:08:03','10.1.8.131',NULL),(174,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:08:49','10.1.8.131',NULL),(175,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:09:05','10.1.8.131',NULL),(176,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:09:36','10.1.8.131',NULL),(177,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:10:39','10.1.8.131',NULL),(178,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:11:06','10.1.8.131',NULL),(179,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:12:06','10.1.8.131',NULL),(180,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:12:11','10.1.8.131',NULL),(181,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:12:14','10.1.8.131',NULL),(182,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:13:22','10.1.8.131',NULL),(183,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:13:31','10.1.8.131',NULL),(184,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:13:46','10.1.8.131',NULL),(185,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:14:55','10.1.8.131',NULL),(186,'gp-dt-13.rcsi-internal.ie/DECIDE/index.php',1,'2016-01-27 12:15:31','10.1.8.131',NULL),(187,'/DECIDE/index.php',1,'2016-01-27 12:15:56','10.1.8.131',NULL),(188,'/DECIDE/Includes/session.php',1,'2016-01-27 12:15:58','10.1.8.131',NULL),(189,'/DECIDE/index.php',1,'2016-01-27 12:16:47','10.1.8.131',NULL),(190,'/DECIDE/Includes/session.php',1,'2016-01-27 12:16:49','10.1.8.131',NULL),(191,'/DECIDE/index.php',1,'2016-01-27 12:17:13','10.1.8.131',NULL),(192,'/DECIDE/Includes/session.php',1,'2016-01-27 12:17:15','10.1.8.131',NULL),(193,'/DECIDE/patient.php',1,'2016-01-27 12:17:16','10.1.8.131',NULL),(194,'/DECIDE/index.php',1,'2016-01-27 12:17:55','10.1.8.131',NULL),(195,'/DECIDE/index.php',1,'2016-01-27 12:17:56','10.1.8.131',NULL),(196,'/DECIDE/Includes/session.php',1,'2016-01-27 12:17:59','10.1.8.131',NULL),(197,'/DECIDE/patient.php',1,'2016-01-27 12:18:00','10.1.8.131',NULL),(198,'/DECIDE/patient.php',1,'2016-01-27 12:18:24','10.1.8.131',NULL),(199,'/DECIDE/patient.php',1,'2016-01-27 12:19:30','10.1.8.131',NULL),(200,'/DECIDE/index.php',1,'2016-01-27 12:20:03','10.1.8.131',NULL),(201,'/DECIDE/Includes/session.php',1,'2016-01-27 12:20:05','10.1.8.131',NULL),(202,'/DECIDE/patient.php',1,'2016-01-27 12:20:06','10.1.8.131',NULL),(203,'/DECIDE/index.php',1,'2016-01-27 12:42:56','10.1.8.131',NULL),(204,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(205,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(206,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(207,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(208,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(209,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(210,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(211,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(212,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(213,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(214,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(215,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(216,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(217,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(218,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(219,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(220,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(221,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(222,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(223,'/DECIDE/patient.php',1,'2016-01-27 12:42:57','10.1.8.131',NULL),(224,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(225,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(226,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(227,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(228,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(229,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(230,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(231,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(232,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(233,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(234,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(235,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(236,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(237,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(238,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(239,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(240,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(241,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(242,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(243,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(244,'/DECIDE/patient.php',1,'2016-01-27 12:42:59','10.1.8.131',NULL),(245,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(246,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(247,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(248,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(249,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(250,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(251,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(252,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(253,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(254,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(255,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(256,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(257,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(258,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(259,'/DECIDE/patient.php',1,'2016-01-27 12:43:00','10.1.8.131',NULL),(260,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(261,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(262,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(263,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(264,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(265,'/DECIDE/patient.php',1,'2016-01-27 12:43:01','10.1.8.131',NULL),(266,'/DECIDE/index.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(267,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(268,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(269,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(270,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(271,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(272,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(273,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(274,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(275,'/DECIDE/patient.php',1,'2016-01-27 12:43:04','10.1.8.131',NULL),(276,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(277,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(278,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(279,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(280,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(281,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(282,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(283,'/DECIDE/index.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(284,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(285,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(286,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(287,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(288,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(289,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(290,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(291,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(292,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(293,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(294,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(295,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(296,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(297,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(298,'/DECIDE/patient.php',1,'2016-01-27 12:43:05','10.1.8.131',NULL),(299,'/DECIDE/patient.php',1,'2016-01-27 12:43:06','10.1.8.131',NULL),(300,'/DECIDE/patient.php',1,'2016-01-27 12:43:06','10.1.8.131',NULL),(301,'/DECIDE/patient.php',1,'2016-01-27 12:43:06','10.1.8.131',NULL),(302,'/DECIDE/patient.php',1,'2016-01-27 12:43:06','10.1.8.131',NULL),(303,'/DECIDE/patient.php',1,'2016-01-27 12:43:06','10.1.8.131',NULL),(304,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(305,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(306,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(307,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(308,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(309,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(310,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(311,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(312,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(313,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(314,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(315,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(316,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(317,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(318,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(319,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(320,'/DECIDE/patient.php',1,'2016-01-27 12:43:07','10.1.8.131',NULL),(321,'/DECIDE/patient.php',1,'2016-01-27 12:43:08','10.1.8.131',NULL),(322,'/DECIDE/patient.php',1,'2016-01-27 12:43:08','10.1.8.131',NULL),(323,'/DECIDE/patient.php',1,'2016-01-27 12:43:08','10.1.8.131',NULL),(324,'/DECIDE/patient.php',1,'2016-01-27 12:43:08','10.1.8.131',NULL),(325,'/DECIDE/patient.php',1,'2016-01-27 12:43:18','10.1.8.131',NULL),(326,'/DECIDE/index.php',1,'2016-01-27 12:43:20','10.1.8.131',NULL),(327,'/DECIDE/index.php',1,'2016-01-27 12:43:21','10.1.8.131',NULL),(328,'/DECIDE/Includes/session.php',1,'2016-01-27 12:43:24','10.1.8.131',NULL),(329,'/DECIDE/Includes/session.php',1,'2016-01-27 12:43:26','10.1.8.131',NULL),(330,'/DECIDE/index.php',1,'2016-01-27 12:44:13','10.1.8.131',NULL),(331,'/DECIDE/Includes/session.php',1,'2016-01-27 12:44:16','10.1.8.131',NULL),(332,'/DECIDE/Includes/session.php',1,'2016-01-27 12:44:18','10.1.8.131',NULL),(333,'/DECIDE/index.php',1,'2016-01-27 12:44:23','10.1.8.131',NULL),(334,'/DECIDE/index.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(335,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(336,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(337,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(338,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(339,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(340,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(341,'/DECIDE/patient.php',1,'2016-01-27 12:50:35','10.1.8.131',NULL),(342,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(343,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(344,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(345,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(346,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(347,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(348,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(349,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(350,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(351,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(352,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(353,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(354,'/DECIDE/patient.php',1,'2016-01-27 12:50:36','10.1.8.131',NULL),(355,'/DECIDE/patient.php',1,'2016-01-27 13:09:04','10.1.8.131',NULL),(356,'/DECIDE/index.php',1,'2016-01-27 13:09:13','10.1.8.131',NULL),(357,'/DECIDE/Includes/session.php',1,'2016-01-27 13:09:16','10.1.8.131',NULL),(358,'/DECIDE/index.php',1,'2016-01-27 13:21:12','10.1.8.131',NULL),(359,'/DECIDE/Includes/session.php',1,'2016-01-27 13:21:14','10.1.8.131',NULL),(360,'/DECIDE/index.php',1,'2016-01-27 13:21:35','10.1.8.131',NULL),(361,'/DECIDE/index.php',1,'2016-01-27 13:21:45','10.1.8.131',NULL),(362,'/DECIDE/index.php',1,'2016-01-27 13:21:55','10.1.8.131',NULL),(363,'/DECIDE/index.php',1,'2016-01-27 13:22:13','10.1.8.131',NULL),(364,'/DECIDE/index.php',1,'2016-01-27 13:22:22','10.1.8.131',NULL),(365,'/DECIDE/index.php',1,'2016-01-27 13:22:41','10.1.8.131',NULL),(366,'/DECIDE/index.php',1,'2016-01-27 13:22:42','10.1.8.131',NULL),(367,'/DECIDE/index.php',1,'2016-01-27 13:22:49','10.1.8.131',NULL),(368,'/DECIDE/index.php',1,'2016-01-27 13:22:49','10.1.8.131',NULL),(369,'/DECIDE/index.php',1,'2016-01-27 13:27:16','10.1.8.131',NULL),(370,'/DECIDE/Includes/session.php',1,'2016-01-27 13:27:23','10.1.8.131',NULL),(371,'/DECIDE/index.php',1,'2016-01-27 13:30:55','10.1.8.131',NULL),(372,'/DECIDE/Includes/session.php',1,'2016-01-27 13:30:58','10.1.8.131',NULL),(373,'/DECIDE/Includes/session.php',1,'2016-01-27 13:30:59','10.1.8.131',NULL),(374,'/DECIDE/Includes/session.php',1,'2016-01-27 13:31:01','10.1.8.131',NULL),(375,'/DECIDE/Includes/session.php',1,'2016-01-27 13:31:02','10.1.8.131',NULL),(376,'/DECIDE/Includes/session.php',1,'2016-01-27 13:31:03','10.1.8.131',NULL),(377,'/DECIDE/index.php',1,'2016-02-01 10:53:31','10.1.8.131',NULL),(378,'/DECIDE/index.php',1,'2016-02-01 10:56:18','10.1.8.131',NULL),(379,'/DECIDE/Includes/session.php',1,'2016-02-01 10:56:22','10.1.8.131',NULL),(380,'/DECIDE/Includes/session.php',1,'2016-02-01 10:56:24','10.1.8.131',NULL),(381,'/DECIDE/Includes/session.php',1,'2016-02-01 10:57:24','10.1.8.131',NULL),(382,'/DECIDE/Includes/session.php',1,'2016-02-01 10:57:27','10.1.8.131',NULL),(383,'/DECIDE/',1,'2016-02-01 10:58:12','10.1.8.131',NULL),(384,'/DECIDE/index.php',1,'2016-02-02 12:06:00','10.1.8.131',NULL),(385,'/DECIDE/Includes/session.php',1,'2016-02-02 12:06:07','10.1.8.131',NULL),(386,'/DECIDE/Includes/session.php',1,'2016-02-02 12:06:09','10.1.8.131',NULL),(387,'/DECIDE/index.php',1,'2016-02-02 12:20:44','10.1.8.131',NULL),(388,'/DECIDE/Includes/session.php',1,'2016-02-02 12:20:47','10.1.8.131',NULL),(389,'/DECIDE/index.php',1,'2016-02-02 12:24:09','10.1.8.131',NULL),(390,'/DECIDE/index.php',1,'2016-02-02 12:25:08','10.1.8.131',NULL),(391,'/DECIDE/Includes/session.php',1,'2016-02-02 12:25:10','10.1.8.131',NULL),(392,'/DECIDE/index.php',1,'2016-02-02 12:29:20','10.1.8.131',NULL),(393,'/DECIDE/Includes/session.php',1,'2016-02-02 12:32:45','10.1.8.131',NULL),(394,'/DECIDE/index.php',1,'2016-02-02 12:32:49','10.1.8.131',NULL),(395,'/DECIDE/Includes/session.php',1,'2016-02-02 12:32:51','10.1.8.131',NULL),(396,'/DECIDE/index.php',1,'2016-02-02 12:33:07','10.1.8.131',NULL),(397,'/DECIDE/Includes/session.php',1,'2016-02-02 12:33:14','10.1.8.131',NULL),(398,'/DECIDE/index.php',1,'2016-02-02 12:33:17','10.1.8.131',NULL),(399,'/DECIDE/Includes/session.php',1,'2016-02-02 12:33:20','10.1.8.131',NULL),(400,'/DECIDE/index.php',1,'2016-02-02 12:34:27','10.1.8.131',NULL),(401,'/DECIDE/index.php',1,'2016-02-02 12:34:28','10.1.8.131',NULL),(402,'/DECIDE/index.php',1,'2016-02-02 12:34:45','10.1.8.131',NULL),(403,'/DECIDE/index.php',1,'2016-02-02 12:35:03','10.1.8.131',NULL),(404,'/DECIDE/Includes/session.php',1,'2016-02-02 12:35:07','10.1.8.131',NULL),(405,'/DECIDE/index.php',1,'2016-02-02 12:35:13','10.1.8.131',NULL),(406,'/DECIDE/index.php',1,'2016-02-02 12:35:53','10.1.8.131',NULL),(407,'/DECIDE/index.php',1,'2016-02-02 12:37:24','10.1.8.131',NULL),(408,'/DECIDE/index.php',1,'2016-02-02 12:37:26','10.1.8.131',NULL),(409,'/DECIDE/index.php',1,'2016-02-02 12:37:28','10.1.8.131',NULL),(410,'/DECIDE/index.php',1,'2016-02-02 12:37:30','10.1.8.131',NULL),(411,'/DECIDE/index.php',1,'2016-02-02 12:37:31','10.1.8.131',NULL),(412,'/DECIDE/index.php',1,'2016-02-02 12:37:48','10.1.8.131',NULL),(413,'/DECIDE/Includes/session.php',1,'2016-02-02 12:37:50','10.1.8.131',NULL),(414,'/DECIDE/index.php',1,'2016-02-02 12:37:52','10.1.8.131',NULL),(415,'/DECIDE/index.php',1,'2016-02-02 12:46:29','10.1.8.131',NULL),(416,'/DECIDE/index.php',1,'2016-02-02 12:46:51','10.1.8.131',NULL),(417,'/DECIDE/Includes/session.php',1,'2016-02-02 12:46:54','10.1.8.131',NULL),(418,'/DECIDE/index.php',1,'2016-02-02 12:46:56','10.1.8.131',NULL),(419,'/DECIDE/Includes/session.php',1,'2016-02-02 12:46:59','10.1.8.131',NULL),(420,'/DECIDE/index.php',1,'2016-02-02 12:47:04','10.1.8.131',NULL),(421,'/DECIDE/index.php',1,'2016-02-02 12:49:04','10.1.8.131',NULL),(422,'/DECIDE/Includes/session.php',1,'2016-02-02 12:56:54','10.1.8.131',NULL),(423,'/DECIDE/index.php',1,'2016-02-02 12:57:04','10.1.8.131',NULL),(424,'/DECIDE/Includes/session.php',1,'2016-02-02 12:57:07','10.1.8.131',NULL),(425,'/DECIDE/index.php',1,'2016-02-02 12:57:08','10.1.8.131',NULL),(426,'/DECIDE/index.php',1,'2016-02-02 13:08:07','10.1.8.131',NULL),(427,'/DECIDE/new_patient.php',1,'2016-02-02 16:27:39','10.1.8.131',NULL),(428,'/DECIDE/new_patient.php',1,'2016-02-02 16:27:53','10.1.8.131',NULL),(429,'/DECIDE/new_patient.php',1,'2016-02-02 16:28:00','10.1.8.131',NULL),(430,'/DECIDE/index.php',1,'2016-02-02 16:28:16','10.1.8.131',NULL),(431,'/DECIDE/new_patient.php',1,'2016-02-02 16:28:20','10.1.8.131',NULL),(432,'/DECIDE/new_patient.php',1,'2016-02-03 14:56:33','10.1.8.131',NULL),(433,'/DECIDE/new_patient_process.php',1,'2016-02-03 14:56:59','10.1.8.131',NULL),(434,'/DECIDE/new_patient_process.php',1,'2016-02-03 14:57:05','10.1.8.131',NULL),(435,'/DECIDE/new_patient.php',1,'2016-02-03 14:58:50','10.1.8.131',NULL),(436,'/DECIDE/new_patient_process.php',1,'2016-02-03 14:59:01','10.1.8.131',NULL),(437,'/DECIDE/new_patient_process.php',1,'2016-02-03 14:59:07','10.1.8.131',NULL),(438,'/DECIDE/new_patient.php',1,'2016-02-03 15:18:44','10.1.8.131',NULL),(439,'/DECIDE/new_patient_process.php',1,'2016-02-03 15:19:17','10.1.8.131',NULL),(440,'/DECIDE/new_patient.php',1,'2016-02-03 16:13:38','10.1.8.131',NULL),(441,'/DECIDE/new_patient.php',1,'2016-02-03 16:15:26','10.1.8.131',NULL),(442,'/DECIDE/new_patient_process.php',1,'2016-02-03 16:15:57','10.1.8.131',NULL),(443,'/DECIDE/new_patient.php',1,'2016-02-03 16:16:21','10.1.8.131',NULL),(444,'/DECIDE/new_patient_process.php',1,'2016-02-03 16:16:48','10.1.8.131',NULL),(445,'/DECIDE/new_patient_process.php',1,'2016-02-03 16:17:24','10.1.8.131',NULL),(446,'/DECIDE/new_patient.php',1,'2016-02-03 16:17:30','10.1.8.131',NULL),(447,'/DECIDE/new_patient_process.php',1,'2016-02-03 16:17:51','10.1.8.131',NULL),(448,'/DECIDE/new_patient_process.php',1,'2016-02-03 16:20:42','10.1.8.131',NULL),(449,'/DECIDE/new_patient.php',1,'2016-02-03 16:39:26','10.1.8.131',NULL),(450,'/DECIDE/new_patient.php',1,'2016-02-03 16:40:08','10.1.8.131',NULL),(451,'/DECIDE/new_patient.php',1,'2016-02-03 16:44:06','10.1.8.131',NULL),(452,'/DECIDE/new_patient.php',1,'2016-02-03 16:45:32','10.1.8.131',NULL),(453,'/DECIDE/new_patient.php',1,'2016-02-03 16:53:13','10.1.8.131',NULL),(454,'/DECIDE/new_patient.php',1,'2016-02-03 16:53:46','10.1.8.131',NULL),(455,'/DECIDE/new_patient.php',1,'2016-02-03 16:54:22','10.1.8.131',NULL),(456,'/DECIDE/new_patient.php',1,'2016-02-03 16:54:46','10.1.8.131',NULL),(457,'/DECIDE/new_patient.php',1,'2016-02-03 17:10:45','10.1.8.131',NULL),(458,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:11:52','10.1.8.131',NULL),(459,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:15:43','10.1.8.131',NULL),(460,'/DECIDE/new_patient.php',1,'2016-02-03 17:15:45','10.1.8.131',NULL),(461,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:15:47','10.1.8.131',NULL),(462,'/DECIDE/new_patient.php',1,'2016-02-03 17:16:20','10.1.8.131',NULL),(463,'/DECIDE/new_patient.php',1,'2016-02-03 17:18:09','10.1.8.131',NULL),(464,'/DECIDE/new_patient.php',1,'2016-02-03 17:30:04','10.1.8.131',NULL),(465,'/DECIDE/new_patient.php',1,'2016-02-03 17:31:07','10.1.8.131',NULL),(466,'/DECIDE/new_patient.php',1,'2016-02-03 17:33:52','10.1.8.131',NULL),(467,'/DECIDE/new_patient.php',1,'2016-02-03 17:34:09','10.1.8.131',NULL),(468,'/DECIDE/new_patient.php',1,'2016-02-03 17:34:43','10.1.8.131',NULL),(469,'/DECIDE/new_patient.php',1,'2016-02-03 17:36:22','10.1.8.131',NULL),(470,'/DECIDE/new_patient.php',1,'2016-02-03 17:36:38','10.1.8.131',NULL),(471,'/DECIDE/new_patient.php',1,'2016-02-03 17:36:46','10.1.8.131',NULL),(472,'/DECIDE/new_patient.php',1,'2016-02-03 17:39:34','10.1.8.131',NULL),(473,'/DECIDE/new_patient.php',1,'2016-02-03 17:48:01','10.1.8.131',NULL),(474,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:48:53','10.1.8.131',NULL),(475,'/DECIDE/new_patient.php',1,'2016-02-03 17:50:40','10.1.8.131',NULL),(476,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:50:42','10.1.8.131',NULL),(477,'/DECIDE/new_patient.php',1,'2016-02-03 17:51:16','10.1.8.131',NULL),(478,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:51:17','10.1.8.131',NULL),(479,'/DECIDE/index.php',1,'2016-02-03 17:52:37','10.1.8.131',NULL),(480,'/DECIDE/index.php',1,'2016-02-03 17:57:07','10.1.8.131',NULL),(481,'/DECIDE/new_patient.php',1,'2016-02-03 17:57:09','10.1.8.131',NULL),(482,'/DECIDE/new_patient.php',1,'2016-02-03 17:57:10','10.1.8.131',NULL),(483,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:57:12','10.1.8.131',NULL),(484,'/DECIDE/index.php',1,'2016-02-03 17:57:14','10.1.8.131',NULL),(485,'/DECIDE/index.php',1,'2016-02-03 17:57:40','10.1.8.131',NULL),(486,'/DECIDE/new_patient.php',1,'2016-02-03 17:57:43','10.1.8.131',NULL),(487,'/DECIDE/new_patient.php',1,'2016-02-03 17:57:44','10.1.8.131',NULL),(488,'/DECIDE/new_patient_process.php',1,'2016-02-03 17:57:46','10.1.8.131',NULL),(489,'/DECIDE/index.php',1,'2016-02-03 17:57:47','10.1.8.131',NULL);
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `med_groups`
--

DROP TABLE IF EXISTS `med_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `med_groups` (
  `ID_med_groups` int(11) NOT NULL AUTO_INCREMENT,
  `Med_group_name` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_med_groups`),
  UNIQUE KEY `ID_med_groups_UNIQUE` (`ID_med_groups`),
  UNIQUE KEY `Med_group_name_UNIQUE` (`Med_group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `med_groups`
--

LOCK TABLES `med_groups` WRITE;
/*!40000 ALTER TABLE `med_groups` DISABLE KEYS */;
INSERT INTO `med_groups` VALUES (2,'Anti-hypertensive medication(s)'),(1,'Glycaemic medication(s)'),(3,'Lipid medication(s)');
/*!40000 ALTER TABLE `med_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meds`
--

DROP TABLE IF EXISTS `meds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meds` (
  `ID_Meds` int(11) NOT NULL,
  `Med_name` varchar(60) NOT NULL,
  `ID_med_groups` int(11) NOT NULL,
  PRIMARY KEY (`ID_Meds`),
  UNIQUE KEY `ID_Meds_UNIQUE` (`ID_Meds`),
  UNIQUE KEY `Med_name_UNIQUE` (`Med_name`),
  KEY `ID_med_groups_FK_idx` (`ID_med_groups`),
  CONSTRAINT `ID_med_groups_FK` FOREIGN KEY (`ID_med_groups`) REFERENCES `med_groups` (`ID_med_groups`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meds`
--

LOCK TABLES `meds` WRITE;
/*!40000 ALTER TABLE `meds` DISABLE KEYS */;
INSERT INTO `meds` VALUES (1,'Metformin',1),(2,'ACE Inhibitor/ARB/Other RAAS system antagonist',2),(3,'Statin',3),(4,'Sulphonylurea',1),(5,'Calcium channel blocker',2),(6,'Fibrate',3),(7,'Thiazolidinedione',1),(8,'Diuretic',2),(9,'Exetamibe',3),(10,'DPP4-Inhibitor',1),(11,'Betablocker',2),(12,'GLP1-Agonist',1),(13,'Other anti-hypertensive drug group',2),(14,'Insulin',1),(15,'Other anti-diabetic group',1);
/*!40000 ALTER TABLE `meds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `ID_patient` int(11) NOT NULL,
  `Age` int(2) DEFAULT NULL,
  `Gender` set('Male','Female') DEFAULT NULL,
  `Duration` int(2) DEFAULT NULL,
  `GMS` enum('True','False') DEFAULT NULL,
  PRIMARY KEY (`ID_patient`),
  UNIQUE KEY `ID_patient_UNIQUE` (`ID_patient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,90,'Female',4,'True'),(2,50,'Male',1,'False'),(3,98,'Female',3,'True'),(5,99,'Male',6,'True'),(6,76,'Male',6,'True'),(7,61,'Female',3,'False'),(8,543,'Male',5,'True');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_data`
--

DROP TABLE IF EXISTS `patient_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_data` (
  `ID_P_Details` int(11) NOT NULL AUTO_INCREMENT,
  `ID_patient` int(11) NOT NULL,
  `BP_Sys` int(11) NOT NULL,
  `BP_Dia` int(11) NOT NULL,
  `HBA1c` decimal(6,2) NOT NULL,
  `Cholesterol` decimal(6,2) NOT NULL,
  `Prev_disease` enum('True','False') NOT NULL,
  `QRisk` decimal(6,2) NOT NULL,
  `Initial_Followup` enum('Initial','FollowUp') NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ID_P_Details`),
  UNIQUE KEY `ID_P_Details_UNIQUE` (`ID_P_Details`),
  KEY `ID_patient_idx` (`ID_patient`),
  CONSTRAINT `ID_patient` FOREIGN KEY (`ID_patient`) REFERENCES `patient` (`ID_patient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_data`
--

LOCK TABLES `patient_data` WRITE;
/*!40000 ALTER TABLE `patient_data` DISABLE KEYS */;
INSERT INTO `patient_data` VALUES (1,6,5,6,4.00,7.00,'True',23.00,'Initial','2016-02-03 16:20:42'),(2,7,44,444,10.00,4444.00,'True',12.00,'Initial','2016-02-03 17:11:52'),(3,8,918,321,613.98,654.09,'True',564.94,'Initial','2016-02-03 17:48:53');
/*!40000 ALTER TABLE `patient_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_meds`
--

DROP TABLE IF EXISTS `patient_meds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_meds` (
  `ID_Patient_Meds` int(11) NOT NULL AUTO_INCREMENT,
  `ID_P_Details` int(11) NOT NULL,
  `ID_Meds` int(11) NOT NULL,
  PRIMARY KEY (`ID_Patient_Meds`),
  UNIQUE KEY `ID_Meds_UNIQUE` (`ID_Patient_Meds`),
  KEY `ID_P_Details_meds_idx` (`ID_P_Details`),
  KEY `ID_Meds_patient_meds_idx` (`ID_Meds`),
  CONSTRAINT `ID_Meds_patient_meds` FOREIGN KEY (`ID_Meds`) REFERENCES `meds` (`ID_Meds`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_P_Details_meds` FOREIGN KEY (`ID_P_Details`) REFERENCES `patient_data` (`ID_P_Details`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_meds`
--

LOCK TABLES `patient_meds` WRITE;
/*!40000 ALTER TABLE `patient_meds` DISABLE KEYS */;
INSERT INTO `patient_meds` VALUES (1,1,1),(2,1,11),(3,1,15),(4,2,11),(5,2,13),(6,2,15),(7,3,1),(8,3,2),(9,3,3);
/*!40000 ALTER TABLE `patient_meds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `ID_Status` int(11) NOT NULL AUTO_INCREMENT,
  `Form` enum('Basic','Details','Recommendations','Decisions') NOT NULL,
  `Status` enum('Save','Submit') NOT NULL,
  `ID_GP` int(11) NOT NULL,
  `ID_patient` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ID_Status`),
  UNIQUE KEY `ID_Status_UNIQUE` (`ID_Status`),
  KEY `ID_GP_status_idx` (`ID_GP`),
  KEY `ID_patient_status_idx` (`ID_patient`),
  CONSTRAINT `ID_GP_status` FOREIGN KEY (`ID_GP`) REFERENCES `gp` (`ID_GP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_patient_status` FOREIGN KEY (`ID_patient`) REFERENCES `patient` (`ID_patient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `ID_Transaction` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `ID_patient` int(11) NOT NULL,
  `ID_GP` int(11) NOT NULL,
  PRIMARY KEY (`ID_Transaction`),
  UNIQUE KEY `ID_Transaction_UNIQUE` (`ID_Transaction`),
  KEY `ID_patient_transactions_idx` (`ID_patient`),
  KEY `ID_gp_transactions_idx` (`ID_GP`),
  CONSTRAINT `ID_gp_transactions` FOREIGN KEY (`ID_GP`) REFERENCES `gp` (`ID_GP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ID_patient_transactions` FOREIGN KEY (`ID_patient`) REFERENCES `patient` (`ID_patient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-04 11:01:31
