from django.contrib import admin
from products.models import Category
from products.models import Product

# Register your models here.
admin.site.register(Category)
admin.site.register(Product)