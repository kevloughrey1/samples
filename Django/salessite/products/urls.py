from django.urls import path
from . views import Homepage
from cart.views import add_product, CartView
app_name= 'products'

urlpatterns = [
    path('', Homepage.as_view(), name='home'),
    path('cart/', CartView, name='cart-home'),
    path('cart/<slug>', add_product, name='cart'),
]