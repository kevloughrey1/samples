from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length = 100)
    desc = models.TextField(max_length = 500)
    price = models.FloatField()
    img = models.CharField(max_length = 50)
    category = models.ForeignKey(Category, on_delete = models.PROTECT)
    slug = models.SlugField()

    def __str__(self):
        return self.name