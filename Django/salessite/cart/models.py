from django.db import models
from django.contrib.auth import get_user_model
from products.models import Product as m_product
# Create your models here.

m_user = get_user_model()

class Cart(models.Model):
    user = models.ForeignKey(m_user, on_delete = models.PROTECT)
    product = models.ForeignKey(m_product, on_delete = models.PROTECT)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return self.user.username

    def sum(self):
        return self.product.price * self.quantity


class Order(models.Model):
    user = models.ForeignKey(m_user, on_delete = models.PROTECT)
    items = models.ManyToManyField(Cart)
    ordered = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    def sum(self):
        return sum(item.sum() for item in self.items.all())