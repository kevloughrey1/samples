from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from .models import Cart, Order
from products.models import Product
# Create your views here.

def add_product(request, slug):
    product_object = get_object_or_404(Product, slug=slug)
    current_item, created = Cart.objects.get_or_create(
        product=product_object
    )
    order_queue = Order.objects.filter(ordered=False)
    if order_queue.exists():
        current_order = order_queue[0]
        if current_order.items.filter(product__slug=product_object.slug).exists():
            current_item.quantity += 1
            current_item.save()
            return redirect("products:home")
        else:
            current_order.items.add(current_item)
            return redirect("products:home")
    else:
        current_order = Order.objects.create()
        current_order.items.add(current_item)
        return redirect("products:home")

def CartView(request):
    carts = Cart.objects.all()
    orders = Order.objects.filter(ordered = False)

    if carts.exists():
        order = orders[0]
        return render(request, 'cart/index.html', {"carts": carts, 'order': order})
    else:
        return redirect("products:home")