from rest_framework import serializers
from mtemplates.models import Activity

class ActivitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Activity
        fields = ('name', 'about')