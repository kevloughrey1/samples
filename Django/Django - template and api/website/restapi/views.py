from django.shortcuts import render
from rest_framework import viewsets

from .serializers import ActivitySerializer
from mtemplates.models import Activity


class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer