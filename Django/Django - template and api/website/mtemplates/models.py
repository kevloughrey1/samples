from django.db import models

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=50)
    about = models.TextField()
    image = models.FilePathField(path="/img")
    def __str__(self):
        return self.name