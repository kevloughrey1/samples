from django.shortcuts import render

# Create your views here.
from mtemplates.models import Activity

def activities(request):
    activities = Activity.objects.all()
    context = {
        'activities': activities
    }
    return render(request, 'activities.html', context)