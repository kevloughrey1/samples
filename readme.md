# Work Samples
## Django
I couldn't find any extant Django projects, so I made two yesterday which highlight basic competencies. One serves a standard html homepage which scales to various device screen sizes. The second uses a templated html page, pulls in data from Django's DB, and has an API. You can run these locally for testing, but if you would like me to deploy these to EC2, just ask.

Update: have made a third - a simple ecommerce site where you can add products to a cart. All Django projects can be run with the standard python3 manage.py runserver command.

## Python
The complete Python API (using Flask) that I developed for an RCSI project is included in the RCSI directory. It's quite a large file, so I've also added a snippets.py file which contains a handful of functions that should give you a decent idea of its quality.

## HTML, JavaScript, and CSS
### LawApp
The first project here is something I'm working on in my spare time. It's in its very very early stages. It is going to be a mobile app which condenses Law Revision notes. Open index.html, then click Flashcards, then View Cards to see it in action.

### Leaderboard
I've included two additional JavaScript and CSS files in the Leaderboard folder. These implement a leaderboard, pulling scores from an online database. Unfortunately I can't send any more code than that as the final product is owned by my current employer and only runs in their LMS, but I have included a video so you can see what it looks like when it's running.


## Docker and Java
A set of Docker and Docker Compose files which deploys another RCSI project. Again, due to IP, I can't include the entire Java API, but I have included a representative class. Also included is a video of the final functionality.

## C++
The ABC4 directory contains very basic game engine that I was working on in my spare time. At the moment, it can render graphics primitives and handle basic 2d physics. Also included is a link to a video of "Plunderer", which is a game I worked on (as team lead) that won the overall award at the 2015 Games Fleadh.

## StackExchange
I have accounts on various StackExchange sites, and enjoy answering questions there in my spare time.
### StackOverflow
https://stackoverflow.com/users/5178006/kevloughrey

### GameDev
https://gamedev.stackexchange.com/users/73614/kevloughrey

### Aggregate Profile
https://stackexchange.com/users/6716267/kevloughrey?tab=accounts